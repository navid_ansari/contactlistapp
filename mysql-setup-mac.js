016-12-15T09:22:09.264502Z 1 [Note] A temporary password is generated for root@localhost: >Kb<rIuP.11I

If you lose this password, please consult the section How to Reset the Root Password in the MySQL reference manual.

//Setup MYSQL on Mac machine


terminal 1

sudo mysqld_safe --skip-grant-tables

terminal 2

sudo mysql -u root
UPDATE mysql.user SET authentication_string=PASSWORD('root') WHERE User='root';
FLUSH PRIVILEGES

close both terminals

open new terminal

sudo mysql -u root -p
new password


cd /usr/local/mysql
sudo ./bin/mysqld_safe

mysql -u root if you have a password setup, or mysql -u root -p root if you don't.

//start mysql

ps aux | grep mysql
sudo kill PID

go to system preferences -- start mysql

mysql -u root -p

//optional - sudo /usr/local/mysql/support-files/mysql.server start

//optional - sudo /usr/local/mysql/support-files/mysql.server stop // if already running

sudo mysqld_safe --skip-grant-tables

// error- Your password has expired. To log in you must change it using a client that supports expired passwords.

ALTER USER root@localhost IDENTIFIED BY 'root';

//never expire password

SET GLOBAL default_password_lifetime = 0;