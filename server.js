var express = require('express'); //npm install --save express
var app = express();
var http = require('http'); //handles http requests
var path = require('path'); //image upload //npm install --save path
var uuid = require("uuid"); // image upload //npm install --save uuid
//var db = require('mongojs').connect('localhost/appDB',['appDB']);
var mongojs = require('mongojs'); //npm install mongojs
var db = mongojs('appDB', ['appDB', 'img_Upload']); //include database in server
//var db = mongojs('appDB', ['img_Upload']);
//var db = mongojs('contactListDB', ['contactListDB']);

var bodyParser = require('body-parser'); //npm install body-parser
var multer = require('multer'); //image upload //npm install multer

var storage = multer.diskStorage({ //include this to save image with random name and extension 
    destination: function (req, file, cb) { // remove this code to store image without extension
        cb(null, 'public/upload')
    },
    filename: function (req, file, cb) {
        cb(null, uuid.v4() + path.extname(file.originalname));
    }
})

var upload = multer({
    storage: storage
}); //reference to the multer file upload location

//npm install --save nodemon 

var bcrypt = require('bcryptjs'); //npm install --save bcryptjs

//var jwt = require('jwt-simple'); //npm install --save jwt-simple
//var JWT_SECRET = 'meanapp';

var jwt = require('jsonwebtoken'); //npm install --save jsonwebtoken
var secret = 'harrypotter';
const nodemailer = require('nodemailer'); //npm install --save nodemailer

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'navidansari25july@gmail.com',
        pass: 'navidengeng'
    }
});
var fs = require('fs');
var pdf = require('html-pdf'); //npm install --save html-pdf

//var paypal = require('paypal-rest-sdk');
//paypal.configure({
//    'mode': 'sandbox', //sandbox or live
//    'client_id': 'EBWKjlELKMYqRNQ6sYvFo64FtaRLRR5BdHEESmha49TM',
//    'client_secret': 'EO422dn3gQLgDbuwqTjzrFgFtaRLRR5BdHEESmha49TM'
//});

var cors = require('cors');

var passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy;

var mysql = require('mysql'); //npm install --save mysql

require('node-import');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'sampleDB'
});

connection.connect(function (error) {
    if (!!error) {
        console.log("Error in MYSQL connection");
    } else {
        console.log("MYSQL connection successfull");
    }
});


//app.get('/', function(req, resp){
//   connection.query("select * from sampleDB", function(error, rows, fields){
//       
//       if(!!error){
//           console.log("error in query");
//       }else{
//           console.log("query success");
//       }
//       
//   });
//});

//var upload = multer({dest:'public/upload'}); //use this to upload image without extension

//test express server
//app.get('/', function (req, res) {
//  res.send('Hello World! its working now')
//})

app.use(express.static(__dirname + "/public")); //to add project folder in server


app.use(bodyParser.json());
//app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(bodyParser.urlencoded({
    extended: true
}));

//app.use(function (req, res, next) { // allow mime type
//
//    // Website you wish to allow to connect
//    res.header("Access-Control-Allow-Origin", "*");
//
//    // Request methods you wish to allow
//    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//
//    // Request headers you wish to allow
//    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//    next();
//});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use(function (req, res, next) { // Error: Can't set headers after they are sent.
    var _send = res.send;
    var sent = false;
    res.send = function (data) {
        if (sent) return;
        _send.bind(res)(data);
        sent = true;
    };
    next();
});

app.use(cors());

//var FACEBOOK_APP_ID = '139265379912961';
//var FACEBOOK_APP_SECRET = '24b816ba141b49b44fe3b07d1e4f78c8';
//
//var fbOpts = {
//    clientID: FACEBOOK_APP_ID,
//    clientSecret: FACEBOOK_APP_SECRET,
//    //callbackURL: 'http://localhost:3000/auth/facebook/callback'
//}
//
//var fbCallback = function(accessToken, rereshToken, profile, cb){
//    
//    console.log(accessToken, rereshToken, profile, cb);
//    
//};

//passport.use(new FacebookStrategy(fbOpts, fbCallback));

//passport.use(new FacebookStrategy({
//    clientID: '139265379912961',
//    clientSecret: '24b816ba141b49b44fe3b07d1e4f78c8',
//    callbackURL: "http://www.google.com"
//  },
//  function(accessToken, refreshToken, profile, done) {
//    
//    console.log("running facebook auth");
//    
//  }
//));
//browser refresh problem //but enabling this will stop the console in terminal
//app.all('/*', function (req, res, next) {
//    // Just send the index.html for other files to support HTML5Mode
//    res.sendFile('public/index.html', {
//        root: __dirname
//    });
//});





// testing controller and server communication using static data (very important)    
//app.get('/userList', function (req, res) {
//    console.log("GET request recieved");
//    person1 = {
//            name: 'Tim',
//            email: 'tim@gmail.com',
//            number: '9028894924'
//        };
//        person2 = {
//            name: 'Jim',
//            email: 'Jim@gmail.com',
//            number: '9822868686'
//        };
//        var contactList = [person1, person2];
//        res.json(contactList);
//});

//app.get('/', function(req, resp){
//   connection.query("select * from sampleDB", function(error, rows, fields){
//       
//       if(!!error){
//           console.log("error in query");
//       }else{
//           console.log("query success");
//       }
//       
//   });
//});

//File Upload
app.post('/upload', upload.any(), function (req, res) { //for upload.single() --> console.log(JSON.stringify(req.file));

    console.log(req); //for upload.any or upload.array() --> console.log(JSON.stringify(req.file));
    console.log(+res);
    console.log(JSON.stringify(req.files));
    console.log(req.files);
    console.log("file name is :" + req.files[0].filename); // or console.log("req.file: " +(req.files[0]).filename); because data in req.files is in array [{'1','2','3}]
    console.log("file path is :" + req.files[0].path);
    console.log(JSON.stringify(req.body));

    // remove 'public' from image path - public\upload\63ed37b2-4b87-4db1-a0a9-21ccc208d54a.png

    var imgPath = req.files[0].path; //store original image path    
    var splitPath = imgPath.split("/")[0]; //split 'public' from path name
    var replacedPath = imgPath.replace(splitPath, ""); //get remaining image path

    console.log("Replaced path is : " + replacedPath);

    //    var a=imgPath.split("/")[0];
    //    var b=imgPath.replace(a,"");
    //    console.log("value of B is :"+b);
    //console.log("file path in variable: " + imgPath);

    res.json({
        success: true
    });
    res.send("Image uploaded successfully!");
    console.log("name is :" + req.body.Name);
    console.log("password is :" + req.body.Password);
    console.log("filepath is :" + req.files[0].path);

    var tempObj = {
        Name: req.body.Name,
        Password: req.body.Password,
        Path: replacedPath
    };

    try {
        db.img_Upload.insert(tempObj, function (err, inserImg) {

            if (err) {
                console.log("error in image insert " + err.message);
            } else {
                //res.json("inserted in image table :" + inserImg);
                console.log("image successfully inserted in database " + JSON.stringify(inserImg));
            }
        })

    } catch (err) {
        console.log("error in image upload to DB :" + err.message);
    }

});

//Image process
app.get('/displayImage/:userName', function (req, res) {

    var name_temp = req.params.userName;
    console.log("name on server :" + name_temp);

    db.img_Upload.findOne({
        _id: mongojs.ObjectId(name_temp)
    }, function (err, doc) {
        // doc._id.toString() === '523209c4561c640000000001' 
        res.json(doc);
        console.log("response image :" + JSON.stringify(doc));
    });

    //    db.img_Upload.find(function (err, getImg) { //db.products.find({"title": "Big Data: Principles and Best Practices of Scalable Realtime Data Systems"})
    //        {
    //            "Name": "Navid"
    //        }
    //        try {
    //            if (err) {
    //                console.log("erro in query :" + err.message);
    //            } else {
    //                console.log("data fetched successfully :" + JSON.stringify(getImg));
    //            }
    //
    //        } catch (err) {
    //            console.log("error in catch :" + err.message);
    //        }
    //    });

    //    db.appDB.find(function (err, docsDB) {
    //        console.log(docsDB);
    //        res.json(docsDB); // or res.json(docsDB.body);
    //    });


});

//Display Prepopulated Data
app.get('/userList', function (req, res) {
    console.log("prepopulate get requet on server");

    connection.query('SELECT * FROM USER_REG', function (err, rows) {

        if (err) {
            console.log("error in fetching all user query: " + err);
        } else {
            //console.log("success in all user query: " + JSON.stringify(rows));
            res.json(rows);

        }


    });

    //    db.appDB.find(function (err, docsDB) {
    //        console.log(docsDB);
    //        res.json(docsDB); // or res.json(docsDB.body);
    //    });

});

//Register New User
app.post('/RegisterNewUser', upload.any(), function (req, res) {
    console.log("Register user request recieved");
    //console.log("req.files: " + req.files);
    //console.log(JSON.stringify(req.files));
    console.log("REGISTER body is :" + JSON.stringify(req.body));
    console.log("file name is :" + req.files[0].filename); // or console.log("req.file: " +(req.files[0]).filename); because data in req.files is in array [{'1','2','3}]
    console.log("file path is :" + req.files[0].path);

    // remove 'public' from image path - public\upload\63ed37b2-4b87-4db1-a0a9-21ccc208d54a.png

    var imgPath = req.files[0].path; //store original image path    
    var splitPath = imgPath.split("/")[0]; //split 'public' from path name
    var replacedPath = imgPath.replace(splitPath, ""); //get remaining image path

    console.log("Replaced path is : " + replacedPath);

    console.log("fname is: " + req.body.fName);
    console.log("lname is: " + req.body.lName);
    console.log("mobile is: " + req.body.mobileNo);
    console.log("mobile is: " + req.body.emailId);
    console.log("DOB is: " + req.body.dateOfBirth);
    console.log("username is: " + req.body.userName);
    console.log("password is: " + req.body.password);
    console.log("con password is: " + req.body.confPassword);
    console.log("image base is: " + req.body.imgbase);
    console.log("image path is: " + replacedPath);

    //encrypt and insert user password
    bcrypt.genSalt(10, function (err, salt) {

        bcrypt.hash(req.body.password, salt, function (err, hash) {
            //Store hash in your password DB. 
            console.log("Password hash is: " + hash);
            connection.query('INSERT INTO USER_REG (FName, LName, MobNo, EmailId, Dob, UserName, UPassword, ConfPassword, Path, Signature) VALUES (?,?,?,?,?,?,?,?,?,?)', [req.body.fName, req.body.lName, req.body.mobileNo, req.body.emailId, req.body.dateOfBirth, req.body.userName, hash, hash, replacedPath, req.body.imgbase], function (err, rows) {
                if (err) {
                    console.log("error in user register query: " + err);

                } else {
                    res.json(rows);
                    console.log("query result: " + JSON.stringify(rows));
                    console.log("user registered successfully");
                }

            });

        });
    });


    //    var tempObj = {
    //        fName: req.body.fName,
    //        lName: req.body.lName,
    //        mobileNo: req.body.mobileNo,
    //        emailId: req.body.emailId,
    //        dateOfBirth: req.body.dateOfBirth,
    //        userName: req.body.userName,
    //        Password: req.body.password,
    //        confPassword: req.body.confPassword,
    //        Path: replacedPath
    //    };
    //    console.log("DATA in OBJ :" + JSON.stringify(tempObj));
    //    res.send("File Uploaded successfully!");
    //    try {
    //        db.appDB.insert(tempObj, function (err, insertDB) {
    //            res.json("inserted data is: " + insertDB);
    //            console.log("error in data insertion: " + err);
    //            console.log("value inserted in database");
    //        })
    //
    //    } catch (err) {
    //        console.log('error in data insert :' + err);
    //    }
});

app.post('/checkEmail', function (req, res) {

    console.log("email in server: " + req.query.email);
    var email = req.query.email;
    if (req.query.email) {
        console.log("inside check use if")
        connection.query("SELECT * FROM USER_REG WHERE EmailId = ?", [email], function (error, user) {

            if (error) {
                res.json({
                    success: false,
                    message: "error in query"
                });
            } else {
                console.log("select email query result: " + user);
                if (user.length == 1) {

                    //                    res.send({email_exists: true});
                    res.json({
                        succes: false,
                        message: "email id already exist"
                    });

                } else {

                    res.json({
                        success: true,
                        message: "email id is unique"
                    });
                }
            }

        });
    }

});
//Delete Contact
app.delete('/deleteContact/:user_id', function (req, res) {

    var delete_id = req.params.user_id;
    console.log(delete_id);
    console.log("delete id on server: " + delete_id);

    connection.query('DELETE FROM USER_REG WHERE UserId = ?', [delete_id], function (error, rows) {

        try {

            if (error) {
                console.log("error in user delete query: " + error);
            } else {
                console.log("success in user delete query: " + JSON.stringify(rows));
                res.json(rows); // if we dont send response , it will not enter in .success condition
            }

        } catch (e) {
            console.log("error in delete catch: " + e.message);
        }


    });

    //    db.appDB.remove({
    //        _id: mongojs.ObjectId(id_temp)
    //    }, function (err, docDelete) {
    //        res.json(docDelete);
    //    });
});

//Edit contact
app.get('/updateContact/:param_id', function (req, res) {

    var edit_id = req.params.param_id;
    console.log("edit id in server :" + edit_id);

    connection.query('SELECT * from USER_REG WHERE UserId = ?', [edit_id], function (error, rows) {
        if (error) {
            console.log("error in edit query: " + error);
        } else {
            console.log("success in edit query: " + rows);
            res.send(rows); //res.json(rows);

        }
    });

    //db code
    //    db.appDB.findOne({
    //        _id: mongojs.ObjectId(id_temp)
    //    }, function (err, docUpdate) {
    //        res.json(docUpdate);
    //        console.log(docUpdate);
    //    });

});

//Update Contact
app.put('/updateUser/:user_id', function (req, res) {

    var update_id = req.params.user_id;
    console.log("update id in server :" + update_id);
    console.log(JSON.stringify(req.body));
    console.log("FNAME: " + req.body[0].FName);
    console.log("LNAME: " + req.body[0].LName);
    console.log("Mob No: " + req.body[0].MobNo);
    console.log("Email: " + req.body[0].EmailId);
    console.log("Dob: " + req.body[0].Dob);
    console.log("User name: " + req.body[0].UserName);
    console.log("Password: " + req.body[0].UPassword);
    console.log("Conf password: " + req.body[0].ConfPassword);
    //console.log("Conf password: "+req.body[0].path);
    //UPDATE App_User_Details SET G_Degree = ?, G_Percent=?, G_Class=?, C_Qual=?, C_Inst=?, C_StartYear=?, C_EndYear=?, C_Board=? WHERE User_ID = ?

    //encrypt and insert user password
    bcrypt.genSalt(10, function (err, salt) {

        bcrypt.hash(req.body[0].UPassword, salt, function (err, hash) {
            // Store hash in your password DB. 
            console.log("update user password hash is: " + hash);

            connection.query("UPDATE USER_REG SET FName=?, LName=?, MobNo=?, EmailId=?, Dob=?, UserName=?, UPassword=?, ConfPassword=? WHERE UserId=?", [req.body[0].FName, req.body[0].LName, req.body[0].MobNo, req.body[0].EmailId, req.body[0].Dob, req.body[0].UserName, hash, hash, update_id], function (err, rows) {
                if (err) {
                    console.log("error in user update query: " + error);

                } else {
                    console.log("success in update query:" + rows);
                }

            });

        });
    });


    //    connection.query("UPDATE USER_REG SET FName=?, LName=?, MobNo=?, EmailId=?, Dob=?, UserName=?, UPassword=?, ConfPassword=? WHERE UserId=?", [req.body[0].FName, req.body[0].LName, req.body[0].MobNo, req.body[0].EmailId, req.body[0].Dob, req.body[0].UserName, req.body[0].UPassword, req.body[0].ConfPassword, update_id], function (error, rows) {
    //        
    //        
    //        
    //        if (error) {
    //            
    //            console.log("error in user update query: " + error);
    //            
    //        } else {
    //            console.log("success in update query:" + rows);
    //        }
    //    });

    //    try {
    //        db.appDB.findAndModify({
    //            query: {
    //                _id: mongojs.ObjectId(id_temp)
    //            },
    //            update: {
    //                $set: {
    //                    fName: req.body.fName,
    //                    lName: req.body.lName,
    //                    mobileNo: req.body.mobileNo,
    //                    emailId: req.body.emailId,
    //                    dateOfBirth: req.body.dateOfBirth,
    //                    userName: req.body.userName,
    //                    Password: req.body.Password,
    //                    confPassword: req.body.confPassword
    //                }
    //            },
    //            new: true
    //        }, function (err, updateuser) {
    //            res.json(updateuser);
    //            console.log("Update transaction succcessfull");
    //        });
    //
    //    } catch (err) {
    //        console.log("error in update :" + err.message)
    //    }
});

//Login user
app.post('/loginUser', function (req, res) {

    var username = req.query.username;
    var password = req.query.password;

    console.log("NAME: " + username);
    console.log("PASSWORD: " + password);


    connection.query("SELECT * FROM USER_REG WHERE UserName = ?", [username], function (error, user) { //connection.query("SELECT * FROM USER_REG WHERE UserName = ? AND UPassword = ?", [username, password], function (error, rows) {

        console.log("login pass 1: " + JSON.stringify(user));
        console.log("login pass 2: " + user[0].UPassword);

        if (error) throw error;

        if (!user) {

            res.json({
                success: false,
                message: "user does not exist in databse.."
            });
        } else if (user) {

            if (password) {

                bcrypt.compare(password, user[0].UPassword, function (err, compareResult) {

                    console.log("password match response: " + compareResult);
                    if (compareResult == true) {

                        var token = jwt.sign({
                            username: user[0].UserName,
                            email: user[0].EmailId
                        }, secret, {
                            expiresIn: '24h'
                        });

                        res.json({
                            success: true,
                            message: "user authenticated..",
                            token: token,
                            username: user[0].UserName
                        });
                    } else if (compareResult == false) {

                        res.json({
                            success: false,
                            message: "user password do not match.."
                        });
                    }

                });

            } else {

                res.json({
                    succes: false,
                    message: "password not provided.."
                });
            }
        }



    });

    //MongoDB query
    //    db.appDB.find({
    //        userName: username,
    //        Password: password
    //    }, function (err, doc) {
    //        res.json(doc);
    //        console.log(JSON.stringify(doc));
    //        console.log(doc.length);
    //        console.log(err);
    //        if (doc.length >= 1) {
    //
    //            console.log("user already registered");
    //
    //        } else {
    //
    //            console.log("user is not registered");
    //        }
    //
    //    });

});

//app.use(function(req, res, next){
//    
//    var token = req.body.token || req.body.query || req.headers['x-access-token'];
//    if(token){
//        
//        jwt.verify(token, secret, function(err, decoded){
//            
//            if (err){
//                
//                res.json({success:false, message:"token expired"});
//            }else{
//                
//                req.decoded = decoded;
//                next();
//            } 
//        });
//        
//    }else{
//        
//        res.json({success:false, message:"no token provided.."});
//    }
//});

app.post('/me', function (req, res, next) {

    console.log("me route running");
    console.log('token from client side: ' + JSON.stringify(req.query.token));

    if (req.query.token) {

        var decoded = jwt.verify(req.query.token, secret);
        console.log(JSON.stringify(decoded));

        res.json({
            success: true,
            message: "decoded token: ",
            decoded
        });

        //        jwt.verify(req.query.token, secret, function(err, decoded){
        //            
        //            console.log(JSON.stringify(decoded));
        //            
        ////            if (err){
        ////                
        ////                res.json({success:false, message:"token expired"});
        ////                
        ////            }else{
        ////                
        ////                req.decoded = decoded;
        ////                console.log("req.decoded: "+JSON.stringify(req.decoded));
        ////                next();
        ////                res.json({success:true, message:"decoded token: ",})
        ////                
        //////                next();
        ////            } 
        //        });

    } else {

        res.json({
            success: false,
            message: "no token provided.."
        });
    }
    //    console.log("decoded data: "+req.decoded);
    //    res.send(req.decoded);

});

//app.post('/auth/facebook', passport.authenticate('facebook'), function(req, res){
//    
//    console.log("fb login request on server");
//    //res.redirect('http://localhost:3000/auth/facebook/callback');
//})

//mySql test
app.get('/mysql', function (req, res) {

    var id = req.query.PID;
    var fname = req.query.FNAME;
    console.log("ID on server: " + id);
    console.log("FNAME on server: " + fname);

    connection.query('SELECT PersonID, FirstName FROM sampleDB where PersonID = ? and FirstName = ?', [id, fname], function (err, rows) {

        if (err) {
            console.log("error in query: " + err);

        } else {
            res.json(rows);
            console.log("query result: " + JSON.stringify(rows));
        }

    });


    //    connection.query('SELECT * FROM sampleDB', function (err, rows) {
    //        // connected! (unless `err` is set)
    //
    //        if (err) {
    //            console.log("error in query: " + err);
    //
    //        } else {
    //            res.json(rows);
    //            console.log("query result: " + JSON.stringify(rows));
    //        }
    //
    //
    //    });

});

//pie chart 
app.get('/ageGroup1', function (req, res) {

    var queryArray = [];

    connection.query("SELECT COUNT(*) AS COUNT FROM USER_REG WHERE EmailId LIKE '%@yahoo.com%'", function (err, rows1) {

        if (err) {

            console.log("error in count 1 query: " + err);
            //res.send(res);

        } else {

            console.log("success in count 1 query: " + JSON.stringify(rows1));
            console.log("success in count 1 query: " + rows1[0].COUNT);
            //res.json(rows1);
            queryArray.push(rows1);
            console.log("res after query: " + JSON.stringify(rows1));
            console.log("query array push 2: " + JSON.stringify(queryArray));
        }

    });

    connection.query("SELECT COUNT(*) AS COUNT FROM USER_REG WHERE EmailId LIKE '%@gmail.com%'", function (err, rows2) {
        if (err) {

            console.log("error in count 2 query: " + err);
            //res.send(res);

        } else {

            console.log("success in count 2 query: " + JSON.stringify(rows2));
            console.log("success in count 2 query: " + rows2[0].COUNT);
            //res.json(rows2);
            queryArray.push(rows2);
            console.log("res after query: " + JSON.stringify(rows2));
            console.log("query array push 2: " + JSON.stringify(queryArray));
        }
    });

    connection.query("SELECT COUNT(*) AS COUNT FROM USER_REG WHERE EmailId LIKE '%@hotmail.com%'", function (err, rows3) {
        if (err) {

            console.log("error in count 3 query: " + err);
            //res.send(res);

        } else {

            console.log("success in count 3 query: " + JSON.stringify(rows3));
            console.log("success in count 3 query: " + rows3[0].COUNT);
            //res.json(rows);
            queryArray.push(rows3);
            console.log("res after query: " + JSON.stringify(rows3));
            console.log("query array push 3: " + JSON.stringify(queryArray));

            console.log("final array after push: " + JSON.stringify(queryArray));
            res.send(queryArray);
        }
    });



});

//delete multiple
app.delete('/deleteMultiple/:deleteMulParams', function (req, res) {

    var deleteMul = req.params;
    console.log("deleteMul: " + JSON.stringify(deleteMul));
    console.log("deleteMul.deleteMulParams: " + JSON.stringify(deleteMul.deleteMulParams));

    var splitMult = deleteMul.deleteMulParams;

    //method 1 - working
    connection.query("DELETE FROM USER_REG WHERE UserId IN (" + deleteMul.deleteMulParams + ")", function (err, rows) {

        if (rows) {

            console.log("record deleted");
            res.send(rows);

        } else {

            console.log("error in delete: " + err);

        }

    });


    //method 2 - some problem
    //    console.log("splitMult: " + JSON.stringify(splitMult));
    //    
    //    var splitted = splitMult.split(",");
    //    console.log("splitted: "+splitted);
    //    console.log("splitted: "+JSON.stringify(splitted));
    //    //var splitlength = splitted.length;
    //    //console.log("splitlength: " + splitlength);
    //
    //    //console.log("splitted: " + JSON.stringify(splitted));
    //
    //    var emptySpace = "";
    //
    //    for (var i = 0; i < splitted.length; i++) {
    //        if (i == 0) {
    //
    //            console.log("for loop IF: " + (emptySpace += "?"));
    //
    //        } else {
    //
    //            console.log("for loop ELSE: " + (emptySpace += "," + "?"));
    //
    //        }
    //
    //        console.log("for loop END: " + emptySpace);
    //    }
    //
    //    console.log("delete multiple UserId on server: " + JSON.stringify(deleteMul.deleteMulParams));
    //
    //    var QUERY = "DELETE FROM USER_REG WHERE UserId IN ("+emptySpace+")";
    //    
    //    console.log("QUERY: " + QUERY);
    //    console.log("splitted value: "+splitted);
    //    
    //    connection.query(QUERY,splitted, function (err, rows) {
    //
    //        if (err) {
    //            console.log("error in delete multiple query: " + err);
    //
    //        } else {
    //            res.json(rows);
    //            console.log("success in delete multiple query: " + JSON.stringify(rows));
    //        }
    //
    //    });



});


app.get('/viewUser', function (req, res) {

    var viewId = req.query.USERID;
    console.log("viewId: " + viewId);

    var QUERY = "SELECT * FROM USER_REG WHERE UserId = " + viewId + "";

    connection.query(QUERY, function (error, rows) {

        if (rows) {

            console.log("suucess in view user query: " + JSON.stringify(rows));
            res.send(rows);

        } else {
            console.log("error in view user query: " + error);
        }

    });

});

//view user
app.get('/viewUser', function (req, res) {

    var viewId = req.query.USERID;
    console.log("viewId: " + viewId);

    var QUERY = "SELECT * FROM USER_REG WHERE UserId = " + viewId + "";

    connection.query(QUERY, function (error, rows) {

        if (rows) {

            console.log("suucess in view user query: " + JSON.stringify(rows));

            res.send(rows);

        } else {
            console.log("error in view user query: " + error);
        }

    });

});

//view user pdf
app.get('/html2pdf', function (req, res) {

    console.log("html to pdf server");

    //    pdf.create(html, options).toFile('public/pdf', function (err, res) {
    //
    //        if (err) {
    //
    //            console.log("error in pdf conversion: " + err.message);
    //
    //        } else {
    //
    //            console.log("success in pdf conversion" + JSON.stringify(res));
    //
    //        }

    //        pdf.create(html).toFile('public/app/viewuser/viewuser.html', function (err, res) {
    //            console.log(res.filename);
    //        });
    //
    //        pdf.create(html).toStream(function (err, stream) {
    //            stream.pipe(fs.createWriteStream('public/pdf'));
    //        });
    //
    //        pdf.create(html).toBuffer(function (err, buffer) {
    //            console.log('This is a buffer:', Buffer.isBuffer(buffer));
    //        });

    //    });



    var html = fs.readFileSync('public/app/viewuser/viewuser.html', 'utf8');
    var options = {
        format: 'Letter'
    };

    pdf.create(html, options).toFile('public/pdf/myPdf.pdf', function (err, res) {
        if (err) return console.log(err);
        console.log("File name is: " + JSON.stringify(res)); // { filename: '/app/businesscard.pdf' }

    });
    //
    //    pdf.create(html).toStream(function (err, stream) {
    //        stream.pipe(fs.createWriteStream('public/pdf/myPdf.pdf'));
    //    });
    //
    //    pdf.create(html).toBuffer(function (err, buffer) {
    //        console.log('This is a buffer:', Buffer.isBuffer(buffer));
    //    });

});

//payment gateway
app.get('/sendPaymentDetails', function (req, res) {

    var cardno = req.query.CARDNO;
    console.log("card no on server: " + cardno);

    var card_data = {
        "type": "visa",
        "number": "4417119669820331",
        "expire_month": "11",
        "expire_year": "2018",
        "cvv2": "123",
        "first_name": "Joe",
        "last_name": "Shopper"
    };

    paypal.creditCard.create(card_data, function (error, credit_card) {
        if (error) {
            console.log(error);
            throw error;
        } else {
            console.log("Create Credit-Card Response");
            console.log("credit card reponse: " + JSON.stringify(credit_card));
        }
    })


});

//stepform personal details
app.post('/personalInfo', function (req, res) {

    var referenceid = req.query.referenceid;
    var country = req.query.country;
    var state = req.query.state;
    var dist = req.query.dist;
    var city = req.query.city;
    var zipcode = req.query.zipcode;
    var latitude = req.query.latitude;
    var longitude = req.query.longitude;

    console.log("REFERENCE ID: " + referenceid);
    console.log("COUNTRY: " + country);
    console.log("STATE: " + state);
    console.log("DIST: " + dist);
    console.log("CITY: " + city);
    console.log("ZIPCODE: " + zipcode);
    console.log("LATITUDE: " + latitude);
    console.log("LONGITUDE: " + longitude);

    var query = 'INSERT INTO USER_INFO (ReferenceId,Country,State,District,City,Zipcode,Lat,Lon) VALUES (?,?,?,?,?,?,?,?)';

    connection.query(query, [referenceid, country, state, dist, city, zipcode, latitude, longitude], function (error, rows) {

        if (rows) {

            console.log("success in personal info: " + JSON.stringify(rows));
            res.send(rows);

        } else {
            console.log("error in personal info: " + error);
        }
        //        connection.close();

    });

});

//stepform bank details
app.post('/bankDetails', function (req, res) {

    console.log("bank details on server: " + JSON.stringify(req.query.bankDetails));
    console.log("bank details on server: " + req.query.bankDetails);
    console.log("bank reference id on server: " + req.query.referenceid);

    //recieved data would be in JSON string not JSON object --> coonvert it into JSON Object
    var jsonString = req.query.bankDetails;
    console.log("type of jsonString: " + typeof (jsonString));
    var obj = JSON.parse(jsonString);
    console.log("type is: " + typeof (obj));
    console.log("type is: " + JSON.stringify(obj));

    var referenceid = req.query.referenceid;
    var ifsc = obj.ifsc;
    var state = obj.state;
    var bankname = obj.bankname;
    var city = obj.city;
    var branch = obj.branch;

    console.log("referenceid: " + referenceid);
    console.log("ifsc: " + ifsc);
    console.log("state: " + state);
    console.log("bankname: " + bankname);
    console.log("city: " + city);
    console.log("branch: " + branch);


    //var query = 'INSERT INTO USER_INFO (IFSC,BankState,BankCity,Bankname,Branchname) VALUES (?,?,?,?,?) WHERE ReferenceId = ?';
    var query = 'UPDATE USER_INFO SET IFSC = ?, BankState = ?, BankCity = ?, Bankname = ?, Branchname = ? WHERE ReferenceId = ?';
    connection.query(query, [ifsc, state, city, bankname, branch, referenceid], function (error, rows) {

        if (rows) {

            console.log("success in bank details query: ");
            res.send(rows);

        } else {

            console.log("error in bank details query" + error);
        }

    });

});

app.get('/resolveData', function (req, res) {

    var query = "SELECT * FROM USER_REG";

    connection.query(query, function (error, rows) {

        if (rows) {

            console.log("success in all user USER_INFO: " + JSON.stringify(rows));

            res.send(rows);

        } else {
            console.log("error in view user query: " + error);
        }

    });
})

app.post('/sendEmail', function (req, res) {
    console.log("email server");

    // setup email data with unicode symbols
    var mailOptions = {
        from: '"Fred Foo 👻" <navidansari25july@gmail.com>', // sender address
        to: 's.sneha@xangars.com', // list of receivers
        subject: 'Hello ✔', // Subject line
        text: 'Hello world ?', // plain text body
        html: '<b>Hello world ?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });

});
//update contact
//app.put('/contactList/:param_id', function (req, res) {
//    var id_temp = req.params.param_id;
//    console.log(req.body.name);
//
//    db.contactListDB.findAndModify({
//        query: {
//            _id: mongojs.ObjectId(id_temp)
//        },
//        update: {
//            $set: {
//                name: req.body.name,
//                email: req.body.email,
//                number: req.body.number
//            }
//        },
//        new: true
//    }, function (err, updateRemoving) {
//        res.json(updateRemoving);
//    });
//
//});

//Display Prepopulated data
//app.get('/userList', function (req, res) {
//    console.log("GET request recieved");
//
//    db.appDB.find(function (err, docsDB) {
//        console.log(docsDB);
//        res.json(docsDB); // or res.json(docsDB.body);
//    });
//});

//edit
//app.get('/contactList/:param_id', function (req, res) {
//    var id_temp = req.params.param_id;
//    console.log(id_temp);
//    db.contactListDB.findOne({
//        _id: mongojs.ObjectId(id_temp)
//    }, function (err, docEditing) {
//        res.json(docEditing);
//    });
//});

//Register new user
//app.post('/RegisterNewUser', function (req, res) {
//    console.log('New User Register request...');
//    console.log(req.body);
//    try {
//        db.appDB.insert(req.body, function (err, insertDB) {
//            res.json("inserted data is :" + insertDB);
//            console.log("error in data insert" + err);
//            console.log("value inserted in Database");
//        });
//
//    } catch (err) {
//        console.log("error in data insert :" + err);
//    }
//});


//app.post('/contactListApp', function (req, res) {
//    console.log(res.body);
//    console.log(req.files);
//    res.json({
//        success: true
//    });
//});
//add contact
//app.get('http://localhost:3000/register', function (req, res) {
//    console.log("GET recieved");
//    
////    var id_temp = req.params.param_id;
////    console.log(id_temp);
//    
////    console.log(req.body);
//
////    db.appDB.insert(req.body, function (err, doc) {
////        console.log("entered in DB");
////        res.json(doc);
////    });
//});

//delete contact
//app.delete('/contactList/:param_id', function (req, res) {
//    var id_temp = req.params.param_id;
//    console.log(id_temp);
//    db.contactListDB.remove({
//        _id: mongojs.ObjectId(id_temp)
//    }, function (err, docRemoving) {
//        res.json(docRemoving);
//    });
//});

//edit contact
//app.get('/contactList/:param_id', function (req, res) {
//    var id_temp = req.params.param_id;
//    console.log(id_temp);
//    db.contactListDB.findOne({
//        _id: mongojs.ObjectId(id_temp)
//    }, function (err, docEditing) {
//        res.json(docEditing);
//    });
//});

//update contact
//app.put('/contactList/:param_id', function (req, res) {
//    var id_temp = req.params.param_id;
//    console.log(req.body.name);
//
//    db.contactListDB.findAndModify({
//        query: {
//            _id: mongojs.ObjectId(id_temp)
//        },
//        update: {
//            $set: {
//                name: req.body.name,
//                email: req.body.email,
//                number: req.body.number
//            }
//        },
//        new: true
//    }, function (err, updateRemoving) {
//        res.json(updateRemoving);
//    });
//
//});

app.all('/*', function (req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('public/index.html', {
        root: __dirname
    });
});

app.listen(3000);
console.log("server running on port 3000");