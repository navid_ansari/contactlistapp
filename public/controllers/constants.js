/*--------- Validation Constants ----------*/

var USERNAME_REGEX = /^[a-zA-Z0-9]*$/;
var EMAIL_REGEX = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.(?:[A-Za-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$/;
//var EMAIL_REGEX = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/; //without including domain extensions
var MOBILE_REGEX = /^[7-9][0-9]{9}$/;

//var console.log = debug;