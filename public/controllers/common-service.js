app.service('commonService', ['$q', '$http', function ($q, $http) {
    console.log("common service running");

    //date difference calculator
    this.calculateAge = function (birthMonth, birthDay, birthYear) {

        console.log("inside calculate function");

        var todayDate = new Date();
        var todayYear = todayDate.getFullYear();
        var todayMonth = todayDate.getMonth();
        var todayDay = todayDate.getDate();

        var age = todayYear - birthYear;

        if (todayMonth < birthMonth - 1) {
            age--;
        }

        if (birthMonth - 1 == todayMonth && todayDay < birthDay) {
            age--;
        }
        return age;
    };

    this.getRandomNumber = function () {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getMilliseconds();
        var random = Math.floor((Math.random() * 100000) + 1000);
        random = '' + random + h + m + s;
        return random;
    };

}]);

app.service('multipartData', ['$http', 'modalService', '$q', '$log','$rootScope', function ($http, modalService, $q, $log,$rootScope) {
    console.log('multipart service is running');

    this.postData = function (uploadURL, registerData, imgBase) {
        var vm = this;
        console.log("inside post service: " + registerData);

        //console.log("inside post service: " + data2);
        //var obj = {imgBase:imgBase};

        $log.info("registerData: " + JSON.stringify(registerData));
        $log.info("img base in common service: " + imgBase);
        //console.log("data 2: "+JSON.stringify(data2));
        console.log("inside post service: " + uploadURL);
        var fd = new FormData();

        for (var key in registerData) {
            fd.append(key, registerData[key]);
            console.log("key is :" + key + "," + registerData[key]);
            //console.log('fd.appned: '+fd.append(key, registerData[key]);)
        }

        fd.append('imgbase', imgBase); //append image base to json

        var dfd = $q.defer();
        $http({
            url: uploadURL,
            method: 'POST',
            data: fd,
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined //'multipart/form-data'
            }
        }).success(function (response) {
            console.log("response is :" + response)
                //this.getUploadResponse = response;
            dfd.resolve("success");


        }).error(function (error) {
            console.log("error is :" + error)
                //dfd.resolve("error");
        });

        return dfd.promise;
    };


}]);



//app.service('authInterceptor', ['loginService', function (loginService) {
//
//    console.log("interceptor running");
//    this.attachHeader = function () {
//
//        var token = loginService.getToken();
//        if (token){
//            
//            config.headers['x-access-token'] = token;
////            return config;
//        } 
//
//        
//    }
//}]);