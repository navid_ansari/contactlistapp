var app = angular.module('app', ['ngMessages', 'ngAnimate', 'ui.router', 'ui.bootstrap', 'chart.js', 'ngCookies', 'angularUtils.directives.dirPagination', 'toastr', 'ngWizard', 'signature', 'ngProgress','angular-media-preview'])



//window.fbAsyncInit = function () {
//    FB.init({
//        appId: '139265379912961',
//        xfbml: true,
//        version: 'v2.8'
//    });
//    FB.AppEvents.logPageView();
//};
//
//(function (d, s, id) {
//    var js, fjs = d.getElementsByTagName(s)[0];
//    if (d.getElementById(id)) {
//        return;
//    }
//    js = d.createElement(s);
//    js.id = id;
//    js.src = "//connect.facebook.net/en_US/sdk.js";
//    fjs.parentNode.insertBefore(js, fjs);
//}(document, 'script', 'facebook-jssdk'));

//convert text to title case i.e Title
app.filter('titlecase', function () {
    return function (s) {
        s = (s === undefined || s === null) ? '' : s;
        return s.toString().toLowerCase().replace(/\b([a-z])/g, function (ch) {
            return ch.toUpperCase();
        });
    };
});

//convert text to title case i.e Title
//app.filter('titlecase', function() {
//    alert("filter running");
//    return function (input) {
//        console.log("filter input: "+input);
//        var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;
//
//        input = input.toLowerCase();
//        return input.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
//            if (index > 0 && index + match.length !== title.length &&
//                match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
//                (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
//                title.charAt(index - 1).search(/[^\s-]/) < 0) {
//                return match.toLowerCase();
//            }
//
//            if (match.substr(1).search(/[A-Z]|\../) > -1) {
//                return match;
//            }
//
//            return match.charAt(0).toUpperCase() + match.substr(1);
//        });
//    }
//});

//custom filter to add new entry to the top of the list
//app.filter('reverse', function () {
//    return function (items) {
//        if (!items) {
//            return;
//        }
//        return items.slice().reverse();
//    }
//});


app.directive('fileModel', ['$parse', function ($parse) {

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                })
            })
        }
    }

  }]);
app.directive('datepickerPopup', function () {
    return {
        restrict: 'EAC',
        require: 'ngModel',
        link: function (scope, element, attr, controller) {
            //remove the default formatter from the input directive to prevent conflict
            controller.$formatters.shift();
        }
    }
});

app.directive('validateCreditcard', function () {
    var creditcardRegex = /^\d{4}-\d{3}-\d{4}$/;
    console.log("credit card directive running");
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, el, attr, ngModelCtrl) {
            ngModelCtrl.$parsers.unshift(function (input) {
                var valid = creditcardRegex.test(input);
                ngModelCtrl.$setValidity('creditcard', valid);
                return valid;
            });
        }
    };
});

app.directive('loading', function () {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="loading"><img src="http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif" width="20" height="20" />LOADING...</div>',
            link: function (scope, element, attr) {
                scope.$watch('loading', function (val) {
                    if (val)
                        $(element).show();
                    else
                        $(element).hide();
                });
            }
        }
    })
    //loader directive
    //app.directive('loading', function () {
    //    return {
    //        restrict: 'E',
    //        replace: true,
    //        //        template: '<div class="loading"><img src="../img/ajax-loader.gif" width="50" height="50" style="margin-right:15px"/>Please wait....</div>',
    //        template: '<div class="loader" style="margin-left:15px;"><span class="loader-span"><span></div>',
    //        link: function (scope, element, attr) {
    //            scope.$watch('loading', function (val) {
    //                if (val)
    //                    $(element).show();
    //                else
    //                    $(element).hide();
    //            });
    //        }
    //    }
    //});

//app.directive('loading', function () {
//      return {
//        restrict: 'E',
//        replace:true,
//        template: '<div class="loading"><img src="http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif" width="20" height="20" />LOADING...</div>',
//        link: function (scope, element, attr) {
//              scope.$watch('loading', function (val) {
//                  if (val)
//                      scope.loadingStatus = 'true';
//                  else
//                      scope.loadingStatus = 'false';
//              });
//        }
//      }
//  })

//credit and debit--> not using
app.filter('validate', [function () {
    return function (ccnumber) {
        if (!ccnumber) {
            return '';
        }
        ccnumber = ccnumber.toString().replace(/\s+/g, '');
        var len = ccnumber.length;
        var cardType, valid;
        mul = 0,
            prodArr = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]],
            sum = 0;
        while (len--) {
            sum += prodArr[mul][parseInt(ccnumber.charAt(len), 10)];
            mul ^= 1;
        }

        if (sum % 10 === 0 && sum > 0) {
            valid = "valid"
        } else {
            valid = "not valid"
        }

        if (/^(34)|^(37)/.test(ccnumber)) {
            cardType = "American Express";
        }
        if (/^(62)|^(88)/.test(ccnumber)) {
            cardType = "China UnionPay";
        }
        if (/^30[0-5]/.test(ccnumber)) {
            cardType = "Diners Club Carte Blanche";
        }
        if (/^(2014)|^(2149)/.test(ccnumber)) {
            cardType = "Diners Club enRoute";
        }
        if (/^36/.test(ccnumber)) {
            cardType = "Diners Club International";
        }
        if (/^(6011)|^(622(1(2[6-9]|[3-9][0-9])|[2-8][0-9]{2}|9([01][0-9]|2[0-5])))|^(64[4-9])|^65/.test(ccnumber)) {
            cardType = "Discover Card";
        }
        if (/^35(2[89]|[3-8][0-9])/.test(ccnumber)) {
            cardType = "JCB";
        }
        if (/^(6304)|^(6706)|^(6771)|^(6709)/.test(ccnumber)) {
            cardType = "Laser";
        }
        if (/^(5018)|^(5020)|^(5038)|^(5893)|^(6304)|^(6759)|^(6761)|^(6762)|^(6763)|^(0604)/.test(ccnumber)) {
            cardType = "Maestro";
        }
        if (/^5[1-5]/.test(ccnumber)) {
            cardType = "MasterCard";
        }
        if (/^4/.test(ccnumber)) {
            cardType = "Visa"
        }
        if (/^(4026)|^(417500)|^(4405)|^(4508)|^(4844)|^(4913)|^(4917)/.test(ccnumber)) {
            cardType = "Visa Electron"
        }
        return ccnumber + " is a(n) " + cardType + " and it's " + valid;
    };
}]);

//password strength

app.directive("passwordStrength", function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.passwordStrength, function (value) {
                console.log(value);
                if (angular.isDefined(value)) {
                    if (value.length > 8) {
                        scope.strength = 'strong';
                    } else if (value.length > 3) {
                        scope.strength = 'medium';
                    } else {
                        scope.strength = 'weak';
                    }
                }
            });
        }
    };
});

//password strength 
app.directive('checkStrength', function () {
    //console.log("watch working");
    var vm = this;
    //alert('directive running');
    return {
        replace: false,
        restrict: 'EACM',
        link: function (scope, iElement, iAttrs) {

            var strength = {
                colors: ['#F00', '#F90', '#FF0', '#9F0', '#0F0'],
                mesureStrength: function (p) {

                    var _force = 0;
                    var _regex = /[$-/:-?{-~!"^_`\[\]]/g;

                    var _lowerLetters = /[a-z]+/.test(p);
                    var _upperLetters = /[A-Z]+/.test(p);
                    var _numbers = /[0-9]+/.test(p);
                    var _symbols = _regex.test(p);

                    var _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];
                    var _passedMatches = $.grep(_flags, function (el) {
                        return el === true;
                    }).length;
                    if (!!p && p.length > 0) {
                        _force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
                        _force += _passedMatches * 10;

                        // penality (short password)
                        _force = (p.length <= 6) ? Math.min(_force, 10) : _force;

                        // penality (poor variety of characters)
                        _force = (_passedMatches == 1) ? Math.min(_force, 10) : _force;
                        _force = (_passedMatches == 2) ? Math.min(_force, 20) : _force;
                        _force = (_passedMatches == 3) ? Math.min(_force, 40) : _force;
                    } else {
                        _force = '';
                    }
                    return _force;

                },
                getColor: function (s) {

                    var idx = 0;
                    if (s <= 10) {
                        idx = 0;
                    } else if (s <= 20) {
                        idx = 1;
                    } else if (s <= 30) {
                        idx = 2;
                    } else if (s <= 40) {
                        idx = 3;
                    } else {
                        idx = 4;
                    }

                    return {
                        idx: idx + 1,
                        col: this.colors[idx]
                    };

                }
            };



            scope.$watch(iAttrs.checkStrength, function () {
                //alert('watch running');
                if (scope.pw === '') {
                    iElement.css({
                        "display": "none"
                    });
                } else {
                    var c = strength.getColor(strength.mesureStrength(scope.pw));
                    iElement.css({
                        "display": "inline"
                    });
                    iElement.children('li')
                        .css({
                            "background": "#DDD"
                        })
                        .slice(0, c.idx)
                        .css({
                            "background": c.col
                        });
                }
            });

        },
        template: '<li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li>'
    };

});

//email validation
app.directive('validateEmailRemotely', function ($http, $log, registerService) {
    return {
        restrict: 'A',
        scope: true,
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrls) {
            var ngModel = ctrls;
            var vm = this;
            scope.$watch(attrs.ngModel, function (email) {

                registerService.checkEmail(email).then(
                    function (response) {

                        $log.info("check user success in controller" + JSON.stringify(response));
                        if (response.data.message == "email id already exist") {
                            console.log("ngModel validity false");
                            ctrls.$setValidity('validEmail', false);
                            vm.emailExist = true;

                        } else {
                            ctrls.$setValidity('validEmail', true);
                        }
                    },
                    function (error) {

                        $log.info("check user error in controller" + error);

                    });
            });
        }
    }
});

app.config(['ChartJsProvider', function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
        colours: ['#FF5252', '#FF8A80', '#46BFBD', '#00ADF9', '#803690', '#FDB45C'],
        responsive: true
    });
    // Configure all line charts
    ChartJsProvider.setOptions('Line', {
        datasetFill: false
    });
  }]);

//app.config(function ($httpProvider) {
//
//    $httpProvider.interceptors.push('authInterceptor');
//});

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider
        .state("home", {
            url: "/home",
            templateUrl: "app/home/home.html",
            controller: "homeCtrl as HC",
            data: {
                authenticated: true
            }, // this property will apply to all children of 'app'
            resolve: {
                resolveData: ['homeService', function (homeService) {
                    return homeService.getResolveData();

                }]
            }

        })
        .state("register", {
            url: "/register",
            templateUrl: "app/register/register.html",
            controller: "regCtrl as RC",
            data: {
                authenticated: false
            }
        })
        .state("imgUpload", {
            url: "/imgUpload",
            templateUrl: "app/imageupload/imageupload.html",
            controller: "imgCtrl as IC"
        })
        .state("update", {
            url: "/update",
            templateUrl: "app/update/update.html",
            controller: "updateCtrl as UC",
            params: {
                ID: null
            },
            resolve: {
                ID: ['$stateParams', function ($stateParams) {
                    console.log($stateParams);
                    //alert("username from login page"+$stateParams.USERNAME);
                    return $stateParams.ID;
                    //console.log(ID);
                }]
            }
        })
        .state("dropDown", {
            url: "/dropDown",
            templateUrl: "app/dropdown/dropdown.html",
            controller: "countryCtrl as CC"

        })
        .state("login", {
            url: "/login",
            templateUrl: "app/login/login.html",
            controller: "loginCtrl as LC",
            data: {
                authenticated: false
            }
        })
        .state("ecommerce", {
            url: "/ecommerce",
            templateUrl: "app/ecommerce/ecommerce.html",
            controller: "ecommCtrl as EC"

        })
        .state("mysql", {
            url: "/mysql",
            templateUrl: "app/mysql/mysql.html",
            controller: "mysqlCrtl as MC"

        })
        .state("rest", {
            url: "/rest",
            templateUrl: "app/rest/restApi.html",
            controller: "restApiCrtl as AC",
            data: {
                authenticated: true
            }
        })
        .state("viewuser", {
            url: "/viewuser",
            templateUrl: "app/viewuser/viewuser.html",
            params: {
                VIEWUSERDATA: null
            },
            resolve: {
                VIEWUSERDATA: ['$stateParams', function ($stateParams) { //could be different name[VIEWUSERDATA--> paramas, viewuserdata--> resolve]

                    //console.log("view user data in resolve: " + $stateParams.VIEWUSERDATA);
                    console.log("view user data in resolve: " + JSON.stringify($stateParams));
                    return $stateParams.VIEWUSERDATA;

            }]
            },
            controller: "viewCtrl as VC",
        })
        .state("stepform", {
            url: "/stepform",
            abstract: true,
            templateUrl: "app/stepform/stepform.html",
            controller: "stepformCtrl as SC",
            data: {
                authenticated: true
            }

        })
        .state("stepform.profile", {
            url: "/profile",
            templateUrl: "app/stepform/1profile/profile.html"
        })
        .state("stepform.interests", {
            url: "/interests",
            templateUrl: "app/stepform/2interests/interests.html"
        })
        .state("stepform.payment", {
            url: "/payment",
            templateUrl: "app/stepform/3payment/payment.html"
        })
        .state("wizard", {
            url: "/wizard",
            templateUrl: "app/wizard/wizard.html",
            controller: "wizardCtrl as WC"
        });
    // use the HTML5 History API
    //$locationProvider.html5Mode(true);
    //$locationProvider.html5Mode(true);

    //alert('i am route');
    $urlRouterProvider.otherwise("login");
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    }]);

app.run(['$rootScope', '$state', '$location', '$log', 'loginService', 'toastr', function ($rootScope, $state, $location, $log, loginService, toastr) {

    $rootScope.cloakVar = false;

    $rootScope.$on('$stateChangeStart', function (event, toState) {


        var loggedIn = loginService.isLoggedIn();
        if (loggedIn == true) {

            $log.info("Success: user logged in..");

            loginService.getUserFromToken().then(
                function (data) {

                    console.log("success in logged in user data: " + JSON.stringify(data.data));
                    console.log("user name in run: " + data.data.decoded.username);
                    $rootScope.cloakVar = true;
                    console.log("toState: " + JSON.stringify(toState));

                    if (toState.name == "login") {

                        if ($rootScope.toasterFlag) {

                        } else {
                            $rootScope.toasterFlag = true;
                            toastr.info('Welcome', data.data.decoded.username);
                            $state.go('home');
                            $rootScope.hideLogin = true;
                        }

                    }



                },
                function (err) {

                    console.log("error in logged in user data: " + JSON.stringify(err));

                });
            //        });

        } else {

            $log.info("Error: user is not logged in..");
        }



    });

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

        var loggedIn = loginService.isLoggedIn();

        console.log("event: " + event);
        console.log("tostate: " + toState);
        console.log("fromstate: " + fromState);
        console.log(loginService.isLoggedIn());

        if (toState.data.authenticated == true) {

            $log.info("need to be authenticated");
            if (!loginService.isLoggedIn()) {
                event.preventDefault();
                $state.go('login');
            }

        } else if (!toState.data.authenticated == false) {

            $log.info("need not to be authenticated");

        } else {

            $log.info("authentication does not matter");

        }
    });

}]);