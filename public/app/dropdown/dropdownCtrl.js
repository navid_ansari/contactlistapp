app.controller('countryCtrl', ['$scope', '$state', function ($scope, $state) {
    console.log("Country controller running");

    this.CountryCntrl = function () {
        console.log("inside county controller");
        this.countries = {
            'India': {
                'Maharashtra': ['Pune', 'Mumbai', 'Nagpur', 'Akola'],
                'Madhya Pradesh': ['Indore', 'Bhopal', 'Jabalpur'],
                'Rajasthan': ['Jaipur', 'Ajmer', 'Jodhpur']
            },
            'USA': {
                'Alabama': ['Montgomery', 'Birmingham'],
                'California': ['Sacramento', 'Fremont'],
                'Illinois': ['Springfield', 'Chicago']
            },
            'Australia': {
                'New South Wales': ['Sydney'],
                'Victoria': ['Melbourne']
            }
        };
    }
    this.CountryCntrl();


}]);