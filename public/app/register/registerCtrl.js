// register new user controller
app.controller('regCtrl', ['$scope', '$http', '$filter', '$state', 'commonService', 'multipartData', 'loginService', 'modalService', '$rootScope', '$uibModal', '$log', 'registerService', function ($scope, $http, $filter, $state, commonService, multipartData, loginService, modalService, $rootScope, $uibModal, $log, registerService) {

    //console.log("regCtrl called");

    //Constants declaration
    var vm = this;

    //    this.register.password = '';
    this.usernameRegex = USERNAME_REGEX;
    this.emailRegex = EMAIL_REGEX;
    this.mobileRegex = MOBILE_REGEX;

    //    $scope.pw = '';
    //
    //    //    $scope.$watch('pw', function (val) {
    //    //            console.log('running foo'+val);  
    //    //    });
    //
    //    $scope.$watch('pw', function (newVal, oldVal) {
    //        //alert("newVal : " + newVal + " oldVal " + oldVal);
    //        if (newVal.toLowerCase() === 'pause') {
    //            alert('Pause!');
    //            $scope.typeModel = '';
    //        }
    //    });


    //    this.getUpload = function () {
    //
    //            console.log("file onchnage running");
    //        }
    //Register new user
    this.userRegister = function () {
        console.log(this.register);
        var vm = this;

        var pageURL = '/RegisterNewUser';
        console.log("registration data: " + JSON.stringify(this.register));
        $log.info("image data: " + this.base64Img);

        multipartData.postData(pageURL, this.register, this.base64Img).then(function (res) {
            console.log("registration data: " + JSON.stringify(this.register));
            if (res == "success") {
                this.callBack = function () {
                    console.log("registered successfully callback");
                    //                    $state.go('stepform');
                }
                modalService.openModal('Success', 'Registered successfully.', 'OK', this.callBack, null);
                //$state.go('login');

            } else {
                console.log("error in dfd resolve");
            }

        });


        //        $http({
        //            url: '/RegisterNewUser', // No need of IP address
        //            method: 'POST',
        //            data: vm.register
        //                //            headers: {
        //                //                'Content-Type': 'application/x-www-form-urlencoded'
        //                //            }
        //        }).success(function (response) {
        //            console.log(response);
        //            console.log("use registered successfully..");
        //        }).error(function (error) {
        //            console.log("Error inserting the data" + error);
        //        });

        //method 2 for $http.POST
        //        $http.post('/login',vm.fName).success(function(response){
        //           console.log(response) 
        //        });

    };

    this.checkEmail = function (email) {
            console.log("ng change running");
            registerService.checkEmail(email).then(
                function (response) {

                    $log.info("check user success in controller" + JSON.stringify(response));
                    $log.info("check user success in controller" + response.data.message);
                    if(response.data.message == "email id already exist"){
                        vm.showEmailExist = true;
                    }
                    else{
                        vm.showEmailExist = false;
                    }

                },
                function (error) {

                    $log.info("check user error in controller" + error);

                })
        }
        //Date Of Birth Calendar
    this.triggerDate = function () {

        var vm = this;
        console.log("inside triggerDate");
        //var vm = this;
        //        this.today = function () {
        //            this.register.dateOfBirth = new Date();
        //        };
        //        this.today();
        //
        //        this.clear = function () {
        //            this.register.dateOfBirth = null;
        //        };

        this.open2 = function () {
            vm.popup2.opened = true;
        };

        this.popup2 = {
            opened: false
        };

        this.open2();

        this.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        this.dateOptions = {
            //dateDisabled: disabled, //weekend enable disable
            formatYear: 'yyyy', //'yy'
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);

        }

        this.toggleMin = function () { // comment this to disable old date selection
            this.inlineOptions.minDate = this.inlineOptions.minDate ? null : new Date();
            this.dateOptions.minDate = this.inlineOptions.minDate;

        };

        this.toggleMin();

        this.setDate = function (year, month, day) {
            vm.register.dateOfBirth = new Date(year, month, day);
        };

        //$scope.formats = ['dd-MMMM-yyyy'];
        //this.format = 'dd/MM/yyyy';
        //$scope.altInputFormats = ['M!/d!/yyyy'];

        this.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        this.format = this.formats[0];
        this.altInputFormats = ['M!/d!/yyyy'];


        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        //afterTomorrow.setDate(tomorrow.getDate() + 1);
        this.events = [
            {
                date: tomorrow,
                status: 'full'
    },
            {
                date: afterTomorrow,
                status: 'partially'
    }
  ];

        function getDayClass(data) {

            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < vm.events.length; i++) {
                    var currentDay = new Date(vm.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return vm.events[i].status;

                    }

                }

            }

            return '';
        }

    };


    //print date from date input

    this.changeDate = function () {
        //console.log("ng-change running" + date);
        var vm = this;
        console.log("initial date: " + this.register.dateOfBirth);

        var tempDay = this.register.dateOfBirth.getDate();
        var tempMonth = this.register.dateOfBirth.getMonth() + 1; //january is 0
        var tempyear = this.register.dateOfBirth.getFullYear();

        console.log("date: " + tempDay);
        console.log("month: " + tempMonth);
        console.log("year: " + tempyear);

        var age = commonService.calculateAge(tempMonth, tempDay, tempyear);
        console.log("final age is: " + age);

        if (age < 17) {

            //alert("age must be greater than 18");
            this.callBack = function () { // this.callBack is call back function on success

                console.log("success callback running in IF");
            }

            modalService.openModal('Age verification', 'Your age is less than 18 years. Please enter again.', 'OK', this.callBack, 'Cancel'); // this.callBack is call back function on success
            this.register.dateOfBirth = null;
            //modalService.openModal('Are you sure?', 'Are you sure you want to create this round','Ok',this.doSomething, 'Cancel');


        } else {

            //alert("age is greater than 18");
            console.log("selected date is :" + this.register.dateOfBirth.getDate() + "/" + (this.register.dateOfBirth.getMonth() + 1) + "/" + this.register.dateOfBirth.getFullYear());
            var dateConverted = this.register.dateOfBirth.getDate() + "/" + (this.register.dateOfBirth.getMonth() + 1) + "/" + this.register.dateOfBirth.getFullYear(); //convert and parse date in dd/mm/yy format
            console.log("converted date is :" + dateConverted);
            this.register.dateOfBirth = dateConverted; //set converted value in date input

            //var dateTemp = $filter('date')(Date.parse(this.register.dateOfBirth), 'dd-MM-yyyy');
            console.log("date in input: " + this.register.dateOfBirth);
            //this.register.dateOfBirth = $filter('date')(Date.parse(this.register.dateOfBirth), 'dd-MM-yyyy');  
            //console.log("age in db: " + dateConverted);
            //console.log("date in input: " + this.register.dateOfBirth);

            this.callBack = function () { // this.callBack is call back function on success

                console.log("success callback running in ELSE");
            }

            modalService.openModal('Age verification', 'Your are adult. You can avail the policy.', 'Ok', this.callBack, null);
        }

    }

    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
    var weakRegex = '';
    $scope.passwordStrengthRed = {
        "float": "left",
        "width": "25px",
        "height": "3px",
        "margin-top": "2px",
        "transition": "all 0.3s ease"
    };
    $scope.passwordStrengthGreen = {
        "float": "left",
        "width": "25px",
        "height": "3px",
        "margin-top": "2px",
        "transition": "all 0.3s ease"
    };
    $scope.passwordStrengthOrange = {
        "float": "left",
        "width": "25px",
        "height": "3px",
        "margin-top": "2px",
        "transition": "all 0.3s ease"
    };

    this.analyze = function (value) {
        console.log('running');

        if (strongRegex.test(value)) {

            $scope.passwordStrengthGreen["background-color"] = "#5ce05c"; // green
            $scope.passwordStrengthOrange["background-color"] = "#eab149"; //orange
            $scope.passwordStrengthRed["background-color"] = "#ec6161"; //red

        } else {
            if (mediumRegex.test(value)) {
                $scope.passwordStrengthOrange["background-color"] = "#eab149"; //orange
                $scope.passwordStrengthRed["background-color"] = "#ec6161"; //red
                $scope.passwordStrengthGreen["background-color"] = "";
            } else {

                $scope.passwordStrengthRed["background-color"] = "#ec6161"; //red
                $scope.passwordStrengthGreen["background-color"] = "";
                $scope.passwordStrengthOrange["background-color"] = "";
            }
        }


    };

    this.base64Image = false;

    this.signature = function () {

        var vm = this;

        var modalInstance = $uibModal.open({
            templateUrl: 'app/reusable/signatureModal.html',
            controller: 'ModalCtrl as SM'
        });



        $scope.$on('msgid', function (event, base64Sign) {
            console.log("base 64 in reg ctrl: " + base64Sign);
            vm.base64Image = true;
            vm.base64Img = base64Sign;
        });


    }

}]);

app.service('registerService', ['$q', '$log', '$http', function ($q, $log, $http) {

    this.checkEmail = function (email) {

        var vm = this;
        var dfd = $q.defer();
        var req = {
            method: 'POST',
            url: '/checkEmail',
            params: {
                email: email
            }
        }

        $http(req).then(function (response) {

            $log.info("check email success in service: " + response);
            dfd.resolve(response);

        }, function (err) {

            $log.info("check email error in service: " +JSON.stringify(err));
//            dfd.reject(err);

        });

        return dfd.promise;

    }


}]);