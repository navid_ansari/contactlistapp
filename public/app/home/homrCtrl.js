app.controller('homeCtrl', ['$scope', '$rootScope', '$http', '$state', '$cookies', 'modalService', '$timeout', '$log', 'homeService','resolveData', function ($scope, $rootScope, $http, $state, $cookies, modalService, $timeout, $log, homeService,resolveData) { //'$uibModal', $modal

    // testing controller and server communication using static data (very important)    
    //        this.prePopulate = function () {
    //    
    //            var vm = this;
    //            $http({
    //                url: '/userList',
    //                method: 'GET'
    //            }).success(function (response) {
    //                console.log('i got the data I requested');
    //                vm.contactList = response;
    //            });
    //        };
    //    
    //        this.prePopulate();
    //var vm = this;




    //    $timeout(function () {
    //        var vm = this;
    //        $scope.data = [3, 3, 3];
    //    }, 500);

    //var vm =this;

    //drop down sort
    //this.sort = 'FName'; //set default sort filter value // enable this if drop down filter needed

    //sort table by table header(method 1) 
    //this.sortType = 'FName'; // set the default sort type
    //this.sortReverse = false; // set the default sort order
    //this.LName = ''; // set the default search/filter term

    //sort table by table header(method 2)
    //var vm = this;
    //    this.sortColumn = 'FName'; // set the default sort type
    //    this.sortReverse = false; // set the default sort order

    //    this.sortData = function (column) {
    //
    //        vm.sortReverse = (vm.sortColumn == column) ? !vm.sortReverse : false;
    //        vm.sortColumn = column;
    //
    //    };

    //    this.sortClass = function (column) {
    //
    //        if (vm.sortColumn == column) {
    //
    //            return vm.sortReverse ? 'arrow-down' : 'arrow-up'
    //        }
    //
    //        //return '';
    //    };

    //search FName and LName in same input
    //    this.search = function (item) {
    //        var vm = this;
    //        return !!((item.brand.indexOf(vm.search || '') !== -1 || item.model.indexOf(vm.search || '') !== -1));
    //        
    //    }
    //    this.getChange = function () {
    //        console.log("FLName: " + this.search.FLName);
    //        
    //                if(vm.search.FLName == undefined){
    //                    return true;
    //                }else{
    //                    if(vm.search.FName.toLowerCase().indexOf(vm.search.FLName.toLowerCase()) !=-1 || vm.search.LName.toLowerCase().indexOf(vm.search.FLName.toLowerCase()) !=-1){
    //                        return true;
    //                    }
    //                }
    //                return true;
    //    }


    var vm = this;
    
    $rootScope.hideHome = false;
    $rootScope.hideStepform = false;
    $rootScope.hideRestapi = false;
    $rootScope.logout = false;
    $rootScope.hideLogin = false;
    $rootScope.hideRegister = true;
    $rootScope.hideLogin = true;
    
    vm.resolveData = resolveData;
    
    this.dropDownChng = function(){
        
        console.log("selected dropdown value: "+JSON.stringify(this.myDropDown.FName));
    }
    
    $log.info("resolve in controller: "+JSON.stringify(resolveData));
    this.selectAll = false;
    //store UserId in array
    this.selected = [];
    var select = this.selected;

    //show hide all checkbox
    this.showAllCHK = function () {

        //this.selectAll = true;

        if (this.selectAll == false) {
            this.selectAll = true;

        } else {
            this.selectAll = false;
        }

    };

    //checked items
    this.exist = function (item) {

        var vm = this;

        return vm.selected.indexOf(item) > -1;

    }

    //show delete multiple
    this.checkOne = function (userid) {

        var vm = this;

        console.log("clikced item: " + userid);
        var id = vm.selected.indexOf(userid);

        if (id > -1) {

            vm.selected.splice(id, 1);

        } else {

            vm.selected.push(userid);

        }
        console.log("array is: " + vm.selected);
    }

    //delete multiple


    this.deleteChecked = function (select) {
        console.log("inside delete multiple function");

        homeService.deleteChecked(this.selected).then(function (res) {

            if (res) {

                console.log("success in multiple delete controller: ");
                vm.prePopulate();

            }

        });

    };

    this.countAgegroup = function () {
        console.log("count age calling");

        //var vm = this;

        //vm.labels;
        //vm.data;

        homeService.countAgegroup().then(function (res) {
            //var vm = this;
            // this.countData = res;
            // console.log("controller data: " + JSON.stringify(res));

            if (res) {
                console.log("count success in controller" + JSON.stringify(res));

                $scope.data = [];

                for (var i = 0; i < res.length; i++) {

                    res[i][0].COUNT;

                    console.log("length of i: " + res.length);
                    console.log("value of i: " + i);
                    console.log("value in array: " + res[i][0].COUNT);

                    $scope.data.push(res[i][0].COUNT); // method 2 --> for n number of data--> push each value one by one to generate array of graph

                }
                console.log("scope data " + $scope.data);

                //var countData1 = res[0][0].COUNT;
                //var countData2 = res[1][0].COUNT;
                //var countData3 = res[2][0].COUNT;

                //console.log("count 1 data in controller: " + countData1);
                //console.log("count 2 data in controller: " + countData2);
                //console.log("count 3 data in controller: " + countData3);

                $scope.labels = ['Yahoo Mail', 'Gmail', 'Hotmail'];

                //$scope.data = [countData1, countData2, countData3]; //method 1 --> for limited data

            } else {
                console.log("count error in controller");
            }
            //console.log("this.countData outsude: " + countData);



        });

    };

    this.countAgegroup();




    //Display prepopulated data from database
    this.prePopulate = function () {
        console.log("prepopulate function running");
        var vm = this;

        $http.get('/userList')
            .success(
                function (response) {
                    console.log("prepopulate success");
                    $scope.loading = true; 
                    vm.contactList = response; //store query response
                    $scope.loading = false;
                    console.log("all user response: " + JSON.stringify(response));

                    //pagination
                    this.totalItems = vm.contactList.length;

                    if (response) {

                    }
                })
            .error(
                function (error) {
                    console.log("prepopulate error: " + error)
                });

    };


    this.prePopulate(); // auto run function on page load

    //delete contact
    this.removeContact = function (userid) {

        var vm = this;

        console.log("delete id is: " + userid);

        this.callBack = function () { // this.callBack is call back function on success

            console.log("success callback running in IF");
            $http.delete('/deleteContact/' + userid)
                .success(function (response) {
                    console.log("inside delete suceess...");
                    console.log("user deleted successfully: " + JSON.stringify(response));
                    vm.prePopulate();

                }).error(function (error) {

                    console.log("error in user delete: " + error)

                });
        }

        modalService.openModal('Sure?', 'Are you sure to delete?', 'Proceed', this.callBack, 'Cancel').then(function (res) {

            console.log(res);
            if (res == 'succes') {
                //vm.callBack();
                //call the callback method

            } else {


            }

        });

        //        $http.delete('/deleteContact/' + userid)
        //            .success(function (response) {
        //                console.log("inside delete suceess...");
        //                console.log("user deleted successfully: " + JSON.stringify(response));
        //                vm.prePopulate();
        //
        //            }).error(function (error) {
        //
        //                console.log("error in user delete: " + error)
        //
        //            });
    };

    //edit contact
    this.editContact = function (userid) {

        var vm = this;

        console.log("Edit id is: " + userid);
        $state.go('update', {
            ID: userid
        });
    };

    //view contact
    this.viewContact = function (userid) {

        console.log("view user id in controller: " + userid);

        var vm = this;

        homeService.viewContact(userid).then(function (res) {

            if (res) {

                console.log("view controller response: " + JSON.stringify(res));
                vm.viewUserRes = res;
                $state.go('viewuser', {
                    VIEWUSERDATA: vm.viewUserRes
                });
            }

        });

    }

    //this.reverse = true;
    this.sortKey = 'FName';
    this.sort = function (keyname) {

        var vm = this;

        this.sortKey = keyname;
        vm.reverse = !vm.reverse;

    };

    this.logOut = function () {


        this.loginUser = $rootScope.currentUser;
        this.loginUserFName = $rootScope.currentUserFname;

        $log.info('this.loginUser: ' + this.loginUser);
        $log.info('this.loginUserFName: ' + this.loginUserFName);

        $rootScope.token = $cookies.remove('token');
        $rootScope.currentUser = $cookies.remove('currentUser');
        $rootScope.currentUserFname = $cookies.remove('currentUserName');
        $state.go('home');


    };

}]);

app.service('homeService', ['$state', '$q', '$http', 'modalService','$log', function ($state, $q, $http, modalService,$log) {
    console.log("home service called");

    this.countAgegroup = function () {
        console.log("Count age group function called");

        var vm = this;

        var dfd = $q.defer();


        $http.get('/ageGroup1')
            .success(
                function (response) {
                    console.log("age group 1 in success");

                    vm.groupCount1 = response; //store query response
                    console.log("count response: " + JSON.stringify(response));
                    dfd.resolve(response);

                })
            .error(
                function (error) {
                    console.log("age group 1 in success: " + error);
                    //dfd.resolve("error");
                });
        return dfd.promise;
    };

    //delete multiple
    this.deleteChecked = function (selected) {

        console.log("deleteChecked service called ");

        var vm = this;

        var dfd = $q.defer();

        selected = selected;
        console.log("selected UserId in service is: " + selected);

        this.callBack = function () { // this.callBack is call back function on success

            $http.delete('/deleteMultiple/' + selected)
                .success(function (response) {

                    console.log("inside multiple delete success service..");
                    console.log("user deleted successfully: " + JSON.stringify(response));
                    dfd.resolve(response);


                }).error(function (error) {

                    console.log("error in multiple delete service: " + error)

                });
        }

        modalService.openModal('Sure?', 'Are you sure to delete selected ?', 'Proceed', this.callBack, 'Cancel').then(function (res) {

            console.log(res);
            if (res == 'succes') {


            } else {


            }

        });

        return dfd.promise;

    };

    //view contact
    this.viewContact = function (userid) {

        console.log("view user id in service");

        var vm = this;

        var dfd = $q.defer();

        $http.get('/viewUser', {
            params: {
                USERID: userid
            }
        }).success(

            function (success) {

                console.log("view contact controller success: ");
                dfd.resolve(success);

            }).error(function (error) {

            console.log("view contact controller error: " + error.message);
        })


        return dfd.promise;
    }
    
    var vm = this;
    this.getResolveData = function(){
        
        $log.info("getResolveData running: ");
        
        var dfd = $q.defer();
        $http.get('/resolveData').then(
            function(success){
                
                
                $log.info("success in resolve service: "+success);
                vm.resolveData = success.data;
                //$log.info("resolve data in service: "+JSON.stringify(vm.resolveData));
                dfd.resolve(vm.resolveData);
                
            }, function(error){
                
                dfd.reject(error);
                $log.info("error in resolve service: "+error);
                
            });
        
        return dfd.promise;
    }

}]);