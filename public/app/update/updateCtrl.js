//edit controller
app.controller('updateCtrl', ['$scope', '$http', '$state', 'commonService', 'ID', 'modalService', function ($scope, $http, $state, commonService, ID, modalService) {

    console.log("update controller running");
    console.log("ID is :" + ID);
    //    this.headerText = headerText;
    //    this.bodyText = bodyText;
    //    this.buttonText = buttonText;
    //    this.method = method;
    //    this.buttonText2 = buttonText2;
    //
    //    console.log("body text: " + bodyText);
    //    console.log("header text: " + headerText);
    //    console.log("button text: " + buttonText);
    //    console.log("method " + method);
    //    console.log("button text: " + buttonText2);

    this.editUpdate = function () {

        var vm = this;
        console.log("update id is: " + ID);
        $http.get('/updateContact/' + ID).success(function (response) {
            console.log("edit request success" + response);
            console.log(JSON.stringify(response));
            vm.getResponse = response; //get response from server and store in variable
            //console.log("controller response :"+this.getResponse);

        }).error(function (err) {
            console.log("error is :" + err.message);
        });

        // or use .then().error

        //$http.get('/updateContact/'+ID).then(function(response) {
        //    // this callback will be called asynchronously
        //    // when the response is available.
        //    console.log("get request initiated"+response);
        //}, function (err) {
        //    // called asynchronously if an error occurs
        //    // or server returns response with an error status.
        //    console.log("error is :"+err.message);
        //});

    };
    this.editUpdate();

    //    this.dobCalendar = function () {
    //        //console.log("calendar");
    //        this.valuationDatePickerIsOpen = true;
    //
    //        //calculate date differnce(check 18 years criteria) 
    //        var tempDay = this.register.dateOfBirth.getDate();
    //        var tempMonth = this.register.dateOfBirth.getMonth();
    //        var tempyear = this.register.dateOfBirth.getFullYear();
    //        //pass the values in function - difference calculator in common service
    //        var age = commonService.calculateAge(tempDay, tempMonth, tempyear); // fuction definition is declared in common service
    //        console.log("age is :" + age);
    //
    //        if (age < 17) {
    //
    //            alert("age must be greater than 18");
    //            this.register.dateOfBirth = null;
    //
    //        } else {
    //
    //            alert("age is greater than 18");
    //            console.log("selected date is :" + this.register.dateOfBirth.getDate() + "/" + (this.register.dateOfBirth.getMonth() + 1) + "/" + this.register.dateOfBirth.getFullYear());
    //            var dateConverted = this.register.dateOfBirth.getDate() + "/" + (this.register.dateOfBirth.getMonth() + 1) + "/" + this.register.dateOfBirth.getFullYear(); //convert and parse date in dd/mm/yy format
    //            console.log("converted date is :" + dateConverted);
    //            this.register.dateOfBirth = dateConverted; //set converted value in date input 
    //
    //        }
    //    };

    //Date Of Birth Calendar
    this.triggerDate = function () {

        var vm = this;
        console.log("inside triggerDate");
        //var vm = this;
        //        this.today = function () {
        //            this.register.dateOfBirth = new Date();
        //        };
        //        this.today();
        //
        //        this.clear = function () {
        //            this.register.dateOfBirth = null;
        //        };

        this.open2 = function () {
            vm.popup2.opened = true;
        };

        this.popup2 = {
            opened: false
        };

        this.open2();

        this.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        this.dateOptions = {
            //dateDisabled: disabled, //weekend enable disable
            formatYear: 'yyyy', //'yy'
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);

        }

        this.toggleMin = function () { // comment this to disable old date selection
            this.inlineOptions.minDate = this.inlineOptions.minDate ? null : new Date();
            this.dateOptions.minDate = this.inlineOptions.minDate;

        };

        this.toggleMin();

        this.setDate = function (year, month, day) {
            vm.register.dateOfBirth = new Date(year, month, day);
        };

        //$scope.formats = ['dd-MMMM-yyyy'];
        //this.format = 'dd/MM/yyyy';
        //$scope.altInputFormats = ['M!/d!/yyyy'];

        this.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        this.format = this.formats[0];
        this.altInputFormats = ['M!/d!/yyyy'];


        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        //afterTomorrow.setDate(tomorrow.getDate() + 1);
        this.events = [
            {
                date: tomorrow,
                status: 'full'
    },
            {
                date: afterTomorrow,
                status: 'partially'
    }
  ];

        function getDayClass(data) {

            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < vm.events.length; i++) {
                    var currentDay = new Date(vm.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return vm.events[i].status;

                    }

                }

            }

            return '';
        }

    };

    //print date from date input

    this.changeDate = function () {

        console.log("ng-change running");
        var vm = this;
        console.log("initial date: " + this.getResponse[0].Dob);

        var tempDay = this.getResponse[0].Dob.getDate();
        var tempMonth = this.getResponse[0].Dob.getMonth() + 1; //january is 0
        var tempyear = this.getResponse[0].Dob.getFullYear();

        console.log("date: " + tempDay);
        console.log("month: " + tempMonth);
        console.log("year: " + tempyear);

        var age = commonService.calculateAge(tempMonth, tempDay, tempyear);
        console.log("final age is: " + age);

        if (age < 17) {

            //alert("age must be greater than 18");
            this.callBack = function () { // this.callBack is call back function on success

                console.log("success callback running in IF");
            }

            modalService.openModal('Age verification', 'Your age is less than 18 years. Please enter again.', 'OK', this.callBack, 'Cancel'); // this.callBack is call back function on success
            this.getResponse[0].Dob = null;
            //modalService.openModal('Are you sure?', 'Are you sure you want to create this round','Ok',this.doSomething, 'Cancel');


        } else {

            //alert("age is greater than 18");
            console.log("selected date is :" + this.getResponse[0].Dob.getDate() + "/" + (this.getResponse[0].Dob.getMonth() + 1) + "/" + this.getResponse[0].Dob.getFullYear());
            var dateConverted = this.getResponse[0].Dob.getDate() + "/" + (this.getResponse[0].Dob.getMonth() + 1) + "/" + this.getResponse[0].Dob.getFullYear(); //convert and parse date in dd/mm/yy format
            console.log("converted date is :" + dateConverted);
            this.getResponse[0].Dob = dateConverted; //set converted value in date input

            console.log("date in input: " + this.getResponse[0].Dob);
            //this.register.dateOfBirth = $filter('date')(Date.parse(this.register.dateOfBirth), 'dd-MM-yyyy');  

            this.callBack = function () { // this.callBack is call back function on success

                console.log("success callback running in ELSE");
            }

            modalService.openModal('Age verification', 'Your are adult. You can avail the policy.', 'Ok', this.callBack, null);
        }
        console.log("initial date: " + this.getResponse[0].Dob);
    }

    //update user
    this.updateUser = function () {

        console.log("update id is :" + this.getResponse[0].UserId);
        console.log("update password is :" + this.getResponse[0].UPassword);
        console.log("Update data is :" + JSON.stringify(this.getResponse));

        var vm = this;

        $http.put('/updateUser/' + vm.getResponse[0].UserId, vm.getResponse)

        //console.log("password is :"+vm.getResponse.password);

        .success(function (response) {

            console.log("Update user response is :" + response);
            this.callBack = function () { // this.callBack is call back function on success

                console.log("success callback running in IF");
            }
            modalService.openModal('Success', 'Successfully Updated.', 'Ok', this.callBack, null);
            $state.go('home');

        }).error(function (error) {

            console.log("error in Update user :" + error);

        });

    };

    //update contact
    //    $scope.updateContact = function (id) {
    //        console.log("update id is : " + id);
    //        $http.put('/contactList/' + $scope.contact._id, $scope.contact).success(function (response) {
    //            $scope.autoRefreshPage();
    //            $scope.isDisabled = false;
    //        });
    //    };

}]);