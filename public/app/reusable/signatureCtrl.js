//app.controller('modalCtrl', ['$scope', '$uibModalInstance', 'modalService', 'headerText', 'bodyText', 'buttonText', 'method', 'buttonText2', 'titlecaseFilter', '$rootScope', function ($scope, $uibModalInstance, modalService, headerText, bodyText, buttonText, method, buttonText2, titlecaseFilter, $rootScope) {
app.controller('ModalCtrl', ['$scope', '$uibModalInstance','$rootScope','$log', function ($scope, $uibModalInstance,$rootScope,$log) {

    console.log("modal controller running");

    this.saveSignature = function () {
        var vm = this;
        var signature = this.accept();
        vm.base64Sign;

        if (signature.isEmpty) {
            $uibModalInstance.dismiss();
        } else {
            $uibModalInstance.close(signature.dataUrl);
            console.log("base 64: " + signature.dataUrl);
            vm.base64Sign = signature.dataUrl;
            $rootScope.base64Sign2 = vm.base64Sign;
            //console.log('base 64 in variable: '+$rootScope(vm.base64Sign2));
            $rootScope.$broadcast('msgid', vm.base64Sign);
            //$rootScope.$broadcast('msgid1', vm.base64Sign);
        }
        
    }
    
    this.clear = function(){
        $log.info('clear canvas running');
        var vm = this;
        
        if(vm.base64Sign){
            
            $log.info('vm.base64Sign' +vm.base64Sign);
        }
                
    }

    //    this.ok = function () {
    //        $uibModalInstance.close();
    //    };

    this.signatureCancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


}]);

