//app.controller('modalCtrl', ['$scope', '$uibModalInstance', 'modalService', 'headerText', 'bodyText', 'buttonText', 'method', 'buttonText2', 'titlecaseFilter', '$rootScope', function ($scope, $uibModalInstance, modalService, headerText, bodyText, buttonText, method, buttonText2, titlecaseFilter, $rootScope) {
app.controller('modalCtrl', ['$scope', '$uibModalInstance', 'modalService', 'headerText', 'bodyText', 'buttonText', 'method', 'buttonText2', 'titlecaseFilter', '$rootScope', function ($scope, $uibModalInstance, modalService, headerText, bodyText, buttonText, method, buttonText2, titlecaseFilter, $rootScope) {

    this.headerText = headerText;
    this.bodyText = bodyText;
    this.buttonText = buttonText;
    this.method = method;
    this.buttonText2 = buttonText2;

    console.log("body text: " + bodyText);
    console.log("header text: " + headerText);
    console.log("button text: " + buttonText);
    console.log("method " + method);
    console.log("button text: " + buttonText2);

    //var dfd = $q.defer();
    this.ok = function () {

        var vm = this;
        vm.method();
        $uibModalInstance.close();
        //dfd.resolve("success");
    };

    this.cancel = function () {
        var vm = this;
        //vm.method();
        $uibModalInstance.dismiss('cancel');
    };
    
    

}]);

app.service('modalService', ['$http', '$uibModal', '$q', function ($http, $modal, $q) {
    console.log('modal service is running');

    this.openModal = function (headerText, bodyText, buttonText, method, buttonText2) {
        var vm = this;

        //method = method || function(){};
        //buttonText2 = buttonText2 || '';

        console.log("modal function running");

        var dfd = $q.defer();

        var modalInstance = $modal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            controller: 'modalCtrl as PC',
            templateUrl: '../app/reusable/modal.html',
            resolve: {
                headerText: function () {
                    return headerText;
                },
                bodyText: function () {
                    return bodyText;
                },
                buttonText: function () {
                    return buttonText;
                },
                method: function () {
                    return method;
                },
                buttonText2: function () {
                    return buttonText2;
                }
            }
            //dfd.resolve("success");
        });

        dfd.resolve("success");
        return dfd.promise;
    };

    //        return {
    //        alert: alert
    //    };

}]);