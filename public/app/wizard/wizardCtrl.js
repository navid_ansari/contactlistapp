app.controller("wizardCtrl", ['$scope', '$http', 'ifscService', function ($scope, $http, ifscService) {

    var vm = this;

    var userDetailsArray = [];

    this.submit = function () {
        alert("Submitted Wizard!");
    }
    this.addNewStep = function (newStep) {
        this.dynamicSteps.push(newStep);
    }

    this.dynamicSteps = ["step 4"];
    this.dynamicRequiredText = {};

    this.getByZipcode = function () {

        var vm = this;
        //this.address = vm.zipResponse[0].Address;
        console.log("zip code: " + this.zipcode);
        var zipUrl = 'https://www.whizapi.com/api/v2/util/ui/in/indian-city-by-postal-code?pin=' + this.zipcode + '&project-app-key=gffqmiyab5jnw66ydvg67g36';
        console.log("zip url: " + zipUrl);
        $http.get(zipUrl).then(

            function (success) {
                console.log("zip code success: " + JSON.stringify(success.data));
                console.log("zip code success: " + success.data.Data[0].Pincode);
                console.log("zip code success: " + JSON.stringify(success.data.Data));

                vm.zipResponse = success.data.Data;

                console.log("zipResponse: " + vm.zipResponse);
                console.log("zipResponse: " + JSON.stringify(vm.zipResponse));


                vm.country = vm.zipResponse[0].Country;
                vm.state = vm.zipResponse[0].State;
                vm.dist = vm.zipResponse[0].City;

                var userPersonal = {
                    zipcode: vm.zipcode,
                    country: vm.country,
                    state: vm.state,
                    dist: vm.dist
                };
                userDetailsArray.push(userPersonal);

                console.log("userPersonal in personal page object: " + JSON.stringify(userPersonal));
                console.log("userDetailsArray in personal page: " + JSON.stringify(userDetailsArray));

                console.log("vm.Country: " + vm.country);
                console.log("vm.State: " + vm.state);
                console.log("vm.City: " + vm.dist);
            },
            function (error) {
                console.log("zip code error: " + error.message);
            });
    };

    this.getByIfsc = function () {

        console.log("this.bank in controller: " + JSON.stringify(this.bank));
        ifscService.getByIfsc(this.bank).then(

            function (success) {

                if (success) {

                    console.log("success in ifsc controller: " + JSON.stringify(vm.ifscResponse));
                    vm.ifscResponse = success.data.data;
                    console.log("vm.ifscResponse: " + vm.ifscResponse);
                    vm.bank.state = vm.ifscResponse.STATE;
                    vm.bank.bankname = vm.ifscResponse.BANK;
                    vm.bank.city = vm.ifscResponse.CITY;
                    vm.bank.branch = vm.ifscResponse.BRANCH;

                    console.log("this.bank.state: " + vm.bank.state);
                    console.log("this.bank.state: " + vm.bank.bankname);
                    console.log("this.bank.state: " + vm.bank.city);
                    console.log("this.bank.state: " + vm.bank.branch);

                    var userBank = {
                        ifsc: vm.bank.ifsc,
                        state: vm.bank.state,
                        bank: vm.bank.bankname,
                        city: vm.bank.city,
                        branch: vm.bank.branch
                    };

                    userDetailsArray.push(userBank);

                    console.log("userBank in bank detail page: " + JSON.stringify(userBank));
                    console.log("userDetailsArray in bank detail page: " + JSON.stringify(userDetailsArray));

                }
            },
            function (error) {
                console.log("error in ifsc controller");
            });

    };

    this.checkOut = function () {

        console.log("payment details in controller: " + JSON.stringify(this.payment));

        ifscService.checkOut(this.payment).then(
            function (success) {

                console.log("success in payment controller");

            },
            function (error) {

                console.log("error in payment controller");

            });
    };

}]);

app.service("ifscService", ['$q', '$state', '$http', function ($q, $state, $http) {

    this.getByIfsc = function (bankdetails) {

        var vm = this;

        var dfd = $q.defer();

        console.log("ifsc code in service: " + JSON.stringify(bankdetails));

        var ifscUrl = 'http://api.techm.co.in/api/v1/ifsc/' + bankdetails.ifsc;
        console.log("ifsc API in service: " + ifscUrl);

        $http.get(ifscUrl).then(
            function (success) {

                dfd.resolve(success);
                console.log("ifsc success in service: ");

            },
            function (error) {

                dfd.resolve(error);
                console.log("ifsc error in service: ");

            });

        return dfd.promise;
    };

    this.checkOut = function (paymentdetails) {

        console.log("payment details in service: " + JSON.stringify(paymentdetails));

        var dfd = $q.defer();

        $http.get('/sendPaymentDetails', {
            params: {
                CARDNO: paymentdetails.cardno,
                EXPDATE: paymentdetails.expdate,
                CVV2: paymentdetails.cvv2
            }
        }).then(
            function (response) {

                console.log("success in payment details service: ");
                
            },
            function (error) {
                
                console.log("error in payment details service: ");
                
            });

        return dfd.promise;

    };

}]);