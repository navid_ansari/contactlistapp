app.controller('loginCtrl', ['$rootScope', '$scope', '$state', '$http', '$cookies', '$log', 'loginService', '$window', '$location', 'toastr', function ($rootScope, $scope, $state, $http, $cookies, $log, loginService, $window, $location, toastr) {
    //'toastr',
    var vm = this;
    //    $rootScope.toasterFlag = true;
    //    toastr.info('Welcome', res.data.token[0].FName + " " + res.data.token[0].LName);
    $rootScope.hideHome = true;
    $rootScope.hideStepform = true;
    $rootScope.hideRestapi = true;
    $rootScope.logout = true;

    this.loginUser = function () {

        loginService.loginUser(this.login.UNAME, this.login.PASSWORD).then(
            function (res) {

                $log.info("success in login controller: " + JSON.stringify(res.data));
                $log.info("token in controller: " + res.data.token);
                $log.info("user login message: " + res.data.message);
                $scope.loading = true;
                this.userLoginResponse = res;
                $scope.loading = false;
                if (res.data.message == "password not provided..") {

                    toastr.info('password not provided');
                } else if (res.data.message == "user authenticated..") {

                    loginService.setToken(res.data.token);
                    toastr.info('Welcome', res.data.username);
                    $state.go('home');
                }

                $rootScope.hideLogin = true;
                //                 toastr.info('Welcome', res.data.username + " " + res.data.token[0].LName);

            },
            function (err) {

                $log.info("error in login controller: " + err);

            });

    };

    $rootScope.logOut = function () {

        loginService.setToken();
        $rootScope.hideLogin = false;
        $rootScope.hideRegister = false;
    }

    this.sendMail = function () {

        var req = {
            method: 'POST',
            url: '/sendEmail',
        }

        $http(req).then(function (response) {

//            console.log("token sent to server");
//            var decodedToken = response;
//            $log.info('decoded token: ' + JSON.stringify(response));
//            $log.info('decoded username from token: ' + response.data.decoded.username);

        }, function (error) {

//            console.log("error in sending token to server: " + error);

        });

    }
    //            this.loginAsFB = function () {
    //        
    //                var vm = this;
    //        
    //                loginService.loginAsFB().then(
    //                    function (res) {
    //        
    //                        $log.info("success in FB controller: ");
    //        
    //        
    //                    },
    //                    function (err) {
    //        
    //                        $log.info("error in FB controller: " + err);
    //        
    //                    })
    //            }



 }]);

app.service('loginService', ['$state', '$q', '$http', '$window', '$location', '$rootScope', '$log', '$cookies', function ($state, $q, $http, $window, $location, $rootScope, $log, $cookies) {

    var vm = this;

    this.loginUser = function (UNAME, PASSWORD) {

        console.log("inside Login service");
        var vm = this;

        console.log("UNAME in service: " + UNAME);
        console.log("PASSWORD in service: " + PASSWORD);
        //this.currentUserSave = UNAME;
        console.log("current user save in service: " + UNAME);
        // auth0 domain: navideng.auth0.com
        // auth0 client id: cXuueUFrN6UODkzwKII07IO54O3Dj2Px
        var dfd = $q.defer();

        var req = {
            method: 'POST',
            url: '/loginUser',
            headers: {
                'Content-Type': undefined
            },
            params: {
                username: UNAME,
                password: PASSWORD
            }
        }

        $http(req).then(function (response) {

            console.log("Login data sent to server");
            vm.loginResponse = response;
            console.log("login response in service: " + JSON.stringify(vm.loginResponse.data));
            dfd.resolve(response);

        }, function (error) {

            console.log("error in sending login data to server: " + error);
            dfd.resolve(error);

        });


        return dfd.promise;

    }

    //set token in localStorage
    this.setToken = function (token) {

        if (token) {

            $window.localStorage.setItem('token', token);

        } else {

            $window.localStorage.removeItem('token');
        }
    }

    //
    this.getToken = function () {

        console.log($window.localStorage.getItem('token'));
        $rootScope.globalToken = $window.localStorage.getItem('token');
        console.log("token in rootscope: " + $rootScope.globalToken);
        return $window.localStorage.getItem('token');

    }

    this.isLoggedIn = function () {

        var getToken = vm.getToken();

        if (getToken) {

            console.log("token found");
            return true;

        } else {

            return false;
        }
    }

    this.getUserFromToken = function () {

        var dfd = $q.defer();


        if (vm.getToken()) {

            var req = {
                method: 'POST',
                url: '/me',
                headers: {
                    Authorization: $rootScope.globalToken
                },
                params: {
                    token: $rootScope.globalToken
                }
            }

            $http(req).then(function (response) {

                console.log("token sent to server");
                var decodedToken = response;
                $log.info('decoded token: ' + JSON.stringify(response));
                $log.info('decoded username from token: ' + response.data.decoded.username);
                dfd.resolve(response);

            }, function (error) {

                console.log("error in sending token to server: " + error);
                dfd.resolve(error);

            });


        } else {

            $log.info("user has no token");

        }
        return dfd.promise;
    }

    //    this.loginAsFB = function () {
    //
    //        var dfd = $q.defer();
    //
    //
    //        FB.login(function (response) {
    //            if (response.authResponse) {
    //                console.log('Welcome!  Fetching your information.... ');
    //                FB.api('/me', function (response) {
    //                    console.log('Good to see you, ' + response.name + '.');
    //                    $log.info("facebook response: " + JSON.stringify(response));
    //                    $log.info("facebook username: " + response.name);
    //                    $log.info("facebook id: " + response.id);
    //                    $log.info(response);
    //
    //                    $rootScope.accessToken = FB.getAuthResponse().accessToken;
    //                    console.log("access token in root scope: " + $rootScope.accessToken);
    //                    $state.go('home');
    //                    $cookies.put('facebookToken', $rootScope.accessToken);
    //
    //
    //                    dfd.resolve(response);
    //                });
    //            } else {
    //                console.log('User cancelled login or did not fully authorize.');
    //            }
    //        });
    //
    //        return dfd.promise;
    //    }

    

}]);

app.factory('authInterceptor', ['$rootScope', function ($rootScope) {

    authInterceptorFactory = {};
    console.log("interceptor running");
    authInterceptorFactory.attachHeader = function (config) {

        var token = $rootScope.globalToken;
        if (token) config.headers['x-access-token'] = token;

        return config;
    };

    return authInterceptorFactory;
}]);