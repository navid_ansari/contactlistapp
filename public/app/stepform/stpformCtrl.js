app.controller("stepformCtrl", ['$scope', '$state', '$http', '$filter', '$log', 'stepformService', 'titlecaseFilter', '$q', 'commonService', '$rootScope', function ($scope, $state, $http, $filter, $log, stepformService, titlecaseFilter, $q, commonService, $rootScope) {

    console.log("SC controller loaded");

    var dataArray = [];
    var vm = this;

    $rootScope.hideHome = false;
    $rootScope.hideStepform = false;
    $rootScope.hideRestapi = false;
    $rootScope.logout = false;
    $rootScope.hideLogin = false;
    $rootScope.hideRegister = true;


    this.status = 'disabled';
    this.status1 = 'disabled';
    this.status2 = 'disabled';

    this.hideNext = true;

    this.goToSteps = function () {

        this.hideNext = false;
        //        this.status = 'btn-primary';
        $state.go('stepform.profile');
    };


    this.getByZipcode = function () {

        this.personalInfo = [];

        var vm = this;
        //this.address = vm.zipResponse[0].Address;
        console.log("zip code: " + this.zipcode);
        console.log("village : " + this.city);
        var zipUrl = 'https://www.whizapi.com/api/v2/util/ui/in/indian-city-by-postal-code?pin=' + this.zipcode + '&project-app-key=gffqmiyab5jnw66ydvg67g36';
        console.log("zip url: " + zipUrl);
        $http.get(zipUrl).then(

            function (success) {
                console.log("zip code success: " + JSON.stringify(success.data));
                console.log("zip code success: " + success.data.Data[0].Pincode);
                console.log("zip code success: " + JSON.stringify(success.data.Data));

                //$scope.loading = true;
                vm.zipResponse = success.data.Data;
                //$scope.loading = false;

                console.log("zipResponse: " + vm.zipResponse);
                console.log("zipResponse: " + JSON.stringify(vm.zipResponse));


                vm.country = vm.zipResponse[0].Country;
                vm.state = vm.zipResponse[0].State;
                vm.dist = vm.zipResponse[0].City;
                this.city = vm.zipResponse[0].City;

                //google api key --> AIzaSyCXF_0qBAJyBJKucLCxvbyMjJc4_3J5y4o
                $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + vm.zipcode + '&key=AIzaSyCXF_0qBAJyBJKucLCxvbyMjJc4_3J5y4o').then(
                    function (response) {

                        console.log("google maps response is: " + JSON.stringify(response.data));
                        this.latitude = response.data.results[0].geometry.location.lat;
                        this.longitude = response.data.results[0].geometry.location.lng;
                        $log.info('latitude: ' + latitude + " " + longitude);

                        //$log.info("vm.Country: " + vm.country);
                        //$log.info("vm.State: " + vm.state);
                        //$log.info("vm.City: " + vm.dist);
                        //$log.info("vm.City: " + vm.city);
                        $log.info('vm.latitude' + this.latitude);
                        $log.info('vm.longitude' + this.longitude);

                        vm.latitude = this.latitude;
                        vm.longitude = this.longitude;

                        //                        stepformService.getByZipcode(vm.country, vm.state, vm.dist, vm.city, vm.zipcode, this.latitude, this.longitude).then(
                        //                            function (success) {
                        //
                        //                                $log.info("success in personal details controller: ");
                        //                            },
                        //                            function (error) {
                        //
                        //                                $log.info("error in personal details controller: " + error);
                        //
                        //                            });
                    },
                    function (error) {

                        console.log("google maps error: " + error);

                    });

            },
            function (error) {
                console.log("zip code error: " + error.message);
            });


    };


    this.getCity = function (city) {

        $log.info("city from drop down: " + city);


    }

    this.goToInterests = function () {

        console.log("interests function");
        $state.go('stepform.interests');
        //        this.status = 'btn-success';
        this.status = 'active';
        //        this.status1 = 'btn-primary';

        var personalObj = {};
        personalObj.zipcode = this.zipcode;
        personalObj.country = this.country;
        personalObj.state = this.state;
        personalObj.dist = this.dist;
        personalObj.city = this.city;
        personalObj.latitude = vm.latitude;
        personalObj.longitude = vm.longitude;
        this.ReferenceId = commonService.getRandomNumber();

        //$log.info("personalObj: " + JSON.stringify(personalObj, vm.latitude));

        stepformService.goToInterests(personalObj, this.ReferenceId).then(
            function (success) {

                $log.info("success in personal submit controller");

                if (success) {

                    $state.go('stepform.interests');

                }

            },
            function (error) {

                $log.error("error in personal submit controller");

            })

    };

    this.getByIfsc = function () {

        console.log("this.bank in controller: " + JSON.stringify(this.bank));
        stepformService.getByIfsc(this.bank).then(

            function (success) {

                if (success) {

                    console.log("success in ifsc controller: " + JSON.stringify(vm.ifscResponse));
                    vm.ifscResponse = success.data.data;
                    console.log("vm.ifscResponse: " + vm.ifscResponse);
                    vm.bank.state = titlecaseFilter(vm.ifscResponse.STATE);
                    vm.bank.bankname = titlecaseFilter(vm.ifscResponse.BANK);
                    vm.bank.city = titlecaseFilter(vm.ifscResponse.CITY);
                    vm.bank.branch = titlecaseFilter(vm.ifscResponse.BRANCH);

                    //use custom filter in controller -> inject $filter --> inject filtername+Filer i.e 'lowercaseFilter' --> var customFilter = $filter('titlecase')('NAVID'); or var customFilter2 = titlecaseFilter(vm.bank.state);
                    var customFilter = $filter('titlecase')('NAVID');
                    console.log("customFilter: " + customFilter);
                    var customFilter2 = titlecaseFilter(vm.bank.state);
                    console.log("customFilter2: " + customFilter2);

                    //                    vm.bank.state = titlecaseFilter.vm.ifscResponse.STATE;
                    //                    vm.bank.bankname = titlecaseFilter.vm.ifscResponse.BANK;
                    //                    vm.bank.city = titlecaseFilter.vm.ifscResponse.CITY;
                    //                    vm.bank.branch = titlecaseFilter.vm.ifscResponse.BRANCH;

                    console.log("this.bank.state: " + vm.bank.state);
                    console.log("this.bank.state: " + vm.bank.bankname);
                    console.log("this.bank.state: " + vm.bank.city);
                    console.log("this.bank.state: " + vm.bank.branch);

                    vm.status = 'active';
                    //                    vm.status1 = 'btn-primary';


                }
            },
            function (error) {
                console.log("error in ifsc controller");
            });

    };

    this.goToPayment = function () {

        var vm = this;
        console.log("profile function");
        $log.info("reference id in bank details: " + vm.ReferenceId);
        this.status1 = 'active';
        //        this.status2 = 'btn-primary';
        $log.info("bank details in controller: " + JSON.stringify(this.bank));

        stepformService.goToPayment(this.bank, vm.ReferenceId).then(
            function (res) {

                $log.info("success in bank details controller: ");
                vm.bankdetailsRespons = res;
                $log.info("bank details response: " + JSON.stringify(vm.bankdetailsRespons));

                if (!!res) {

                    $state.go('stepform.payment');
                }

            },
            function (err) {

                $log.info("error in bank details controller:" + err);

            });

    };

    this.checkCC = function (value) {


    }

    this.checkOut = function () {

        var vm = this;

        console.log("payment details in controller: " + JSON.stringify(this.payment));

        stepformService.checkOut(this.payment).then(
            function (success) {

                console.log("success in payment controller");

            },
            function (error) {

                console.log("error in payment controller");

            });
    };



}]);

app.service("stepformService", ['$q', '$state', '$http', '$log', 'commonService', function ($q, $state, $http, $log, commonService) {

    this.getByIfsc = function (bankdetails) {

        var vm = this;

        var dfd = $q.defer();

        console.log("ifsc code in service: " + JSON.stringify(bankdetails));

        var ifscUrl = 'http://api.techm.co.in/api/v1/ifsc/' + bankdetails.ifsc;
        console.log("ifsc API in service: " + ifscUrl);

        $http.get(ifscUrl).then(
            function (success) {

                dfd.resolve(success);
                console.log("ifsc success in service: " + JSON.stringify(success));

            },
            function (error) {

                dfd.resolve(error);
                console.log("ifsc error in service: ");

            });

        return dfd.promise;
    };

    this.checkOut = function (paymentdetails) {

        console.log("payment details in service: " + JSON.stringify(paymentdetails));

        var dfd = $q.defer();

        $http.get('/sendPaymentDetails', {
            params: {
                CARDNO: paymentdetails.cardno,
                EXPDATE: paymentdetails.expdate,
                CVV2: paymentdetails.cvv2
            }
        }).then(
            function (response) {

                console.log("success in payment details service: ");

            },
            function (error) {

                console.log("error in payment details service: ");

            });

        return dfd.promise;

    };

    this.goToInterests = function (personalObj, ReferenceId) {

        $log.info("personalObj in service: " + JSON.stringify(personalObj));

        var vm = this;
        var dfd = $q.defer();

        this.country = personalObj.country;
        this.state = personalObj.state;
        this.dist = personalObj.dist;
        this.city = personalObj.city;
        this.zipcode = personalObj.zipcode;
        this.latitude = personalObj.latitude;
        this.longitude = personalObj.longitude;
        this.ReferenceId = ReferenceId;


        $log.info("country in service: " + this.country);
        $log.info("state in service: " + this.state);
        $log.info("dist in service: " + this.dist);
        $log.info("city in service: " + this.city);
        $log.info("zipcode in service: " + this.zipcode);
        $log.info("latitude in service: " + this.latitude);
        $log.info("langitude in service: " + this.longitude)
        $log.info("random no: " + this.ReferenceId);


        var req = {
            method: 'POST',
            url: '/personalInfo',
            headers: {
                'Content-Type': undefined
            },
            params: {

                referenceid: this.ReferenceId,
                country: this.country,
                state: this.state,
                dist: this.dist,
                city: this.city,
                zipcode: this.zipcode,
                latitude: this.latitude,
                longitude: this.longitude
            }
        }

        $http(req).then(function (response) {

            $log.info("personal info request sent to server");

            vm.personalInfoResponse = response;
            $log.info("personal info response in service: " + JSON.stringify(vm.personalInfoResponse.data));
            //            $state.go('stepform.interests',{});
            dfd.resolve(response);


        }, function (error) {

            $log.info("error in sending login data to server: " + error);
            dfd.resolve(error);

        });


        return dfd.promise;

    }

    this.goToPayment = function (bank, referenceid) {

        var vm = this;
        var dfd = $q.defer();

        $log.info("bank details in service: " + JSON.stringify(bank));

        var req = {
            method: 'POST',
            url: '/bankDetails',
            headers: {
                'Content-Type': undefined
            },
            params: {

                bankDetails: bank,
                referenceid: referenceid

            }
        };

        $http(req).then(

            function (response) {

                $log.info("bank details info request sent to server");

                //vm.bankdetailsRespons = response;
                dfd.resolve(response);


            },
            function (error) {

                $log.info("error in sending bank details data to server: " + error);
                dfd.resolve(error);

            });


        return dfd.promise;

    }

    this.checkOut = function () {


    }


}]);