//edit controller
app.controller('restApiCrtl', ['$scope', '$http', '$state', 'commonService', 'restApiService', '$timeout', 'ngProgressFactory','ngProgress','$rootScope', function ($scope, $http, $state, commonService, restApiService, $timeout, ngProgressFactory,ngProgress,$rootScope) {

    console.log("REST controller running");

    //$scope.progressbar = ngProgressFactory.createInstance();
    //$scope.progressbar.setColor('#369c7e');
    //$scope.progressbar.setHeight('4px');
    //$scope.progressbar.status();
    var vm = this;
    
    $rootScope.hideHome = false;
    $rootScope.hideStepform = false;
    $rootScope.hideRestapi = false;
    $rootScope.logout = false;
    $rootScope.hideLogin = true;
    $rootScope.hideRegister = true;

    this.colorCodeArray = [
         "#339E42",
         "#039BE5",
         "#EF6C00",
         "#A1887F",
         "#607D8B",
         "#039BE5",
         "#009688",
    ];

    this.restCall = function () {

        $scope.loading = true;
        restApiService.restCall().then(

            function successCall(response) {
                if (response) {
                    console.log("success in REST API call: ");
                    //$scope.progressbar.start();
                    $scope.loading = true;
                    vm.postResponse = response.data;
                    //$timeout($scope.progressbar.complete(), 3000);
                    $scope.loading = false;
                    console.log("rest api response: " + JSON.stringify(vm.postResponse));

                    console.log("rest api name length: " + JSON.stringify(vm.postResponse.length));



                    vm.displayUsername = vm.postResponse[0]; //first item of dropdown is selected
                }
            },

            function errorCall(error) {
                console.log("error in REST API call: " + error.message);
            });

    };

    this.restCall();

}]);

app.service('restApiService', ['$http', '$q', function ($http, $q) {

    var vm = this;

    this.restCall = function () {

        var dfd = $q.defer();

        try {

            $http.get('https://jsonplaceholder.typicode.com/users').then(

                function successCall(success) {

                    console.log("rest success: ");

                    dfd.resolve(success);

                    console.log("rest data: " + JSON.stringify(vm.postResponse));
                },

                function errorCall(error) {

                    console.log("rest error: " + error.message);

                });

        } catch (e) {

            console.log("error in catch: " + e.message);

        }

        return dfd.promise;
    };

}]);