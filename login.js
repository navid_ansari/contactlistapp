app.post('/loginUser', function (req, res) {

    var username = req.query.username;
    var password = req.query.password;

    console.log("NAME: " + username);
    console.log("PASSWORD: " + password);


    connection.query("SELECT * FROM USER_REG WHERE UserName = ?", [username], function (error, rows) { //connection.query("SELECT * FROM USER_REG WHERE UserName = ? AND UPassword = ?", [username, password], function (error, rows) {

        console.log("login pass 1: " + JSON.stringify(rows));
        console.log("login pass 2: " + rows[0].UPassword);

        if (error) {

            console.log("error in login query: " + error);

        } else {

            console.log("success in login query: ");

            if (rows[0].UPassword) {

                console.log("user password: " + rows[0].UPassword);

                //decrypt and compare user password
                bcrypt.compare(password, rows[0].UPassword, function (err, resp) {
                    // res == true 

                    if (resp) {

                        console.log("JWT_SECRET: " + JWT_SECRET);

                        var token = jwt.encode(resp, JWT_SECRET); //jwt token based login session

                        rows[0].setToken = token; //to send query data and token in one request, assign user data to token variable 

                        res.json({
                            token: rows
                        });
                        console.log("success in password match: " + JSON.stringify(rows));

                    } else {

                        console.log("passwords do not match: " + error);
                        //res.status(400).send();
                        return res.status(400).send({
                            message: 'error'
                        });
                    }

                });
            }
        }

        //res.send(rows);

    });

    //MongoDB query
    //    db.appDB.find({
    //        userName: username,
    //        Password: password
    //    }, function (err, doc) {
    //        res.json(doc);
    //        console.log(JSON.stringify(doc));
    //        console.log(doc.length);
    //        console.log(err);
    //        if (doc.length >= 1) {
    //
    //            console.log("user already registered");
    //
    //        } else {
    //
    //            console.log("user is not registered");
    //        }
    //
    //    });

});

//app.post('/auth/facebook', passport.authenticate('facebook'), function(req, res){
//    
//    console.log("fb login request on server");
//    //res.redirect('http://localhost:3000/auth/facebook/callback');
//})