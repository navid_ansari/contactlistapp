//go to mongodb's bin directory

type 'mongod'

//open new terminal window

type mongo

//show all database name
show dbs 

//create new or use existing with same name
use databaseName 

//insert single value in database
db.databaseName.insert({name:'Tom',email:'tom@gmail.com',number:'9028894924'})

//remove row
db.contactListDB.remove({"name": "Ashwin"})

//insert single value in database
db.databaseName.insert([{name:'Akash',email:'akash@gmail.com',number:'9823584565'},{name:'Nadim',email:'nadim@gmail.com',number:'9270433455'}])

//this.fName, this.lName, this.mobileNo, this.emailId, this.dateOfBirth, this.userName, this.password, this.confPassword
//db.databaseName.insert([{fName:'Akash',lName:'Tripathi',mobileNo:'9823584565',emailId:'akash@gmail.com',dateOfBirth:'25-10-2016',userName:'akash',password:'akash',confPassword:'akash'},])

//see all the data in database //select * from '';
db.databaseName.find()

//beautify the find() ;
db.databaseName.find().pretty()